# **CSSMin**

CSSMin is an extended version of the CSSMin project initiated by 
Joe Scylla on Google Project Hosting (see https://code.google.com/p/cssmin/).


## Requirements:

PHP 5.2.14 should be enough. Nothing really special, works with basic functions.

By convenience, CSSMin consider that there is no error in the CSS.


## How to use it:

At first, call bootstrap:

    :::php
      require $path_to_library.'/CSSMin/bootstrap.inc.php';

Then, minify a CSS file:

    :::php
      $minified_string = CSSMin::minifyFile( $source [ , $settings ] );

Or, minify a CSS string:

    :::php
      $minified_string = CSSMin::minifyString( $source [ , $settings ] );

Or, minify something that can be a CSS file or a CSS string:

    :::php
      $minified_string = CSSMin::minify( $source [ , $settings ] );

I strongly discourage to use the minification without any cache thing behind - nobody wants to call the entire process on each HTTP request.


## Settings:

Default values:

    :::php
      $settings = array(
        'embed_basedir'             => false,
        'embed_maxsize'             => 3072,
        'embed_iefix'               => false,
        'scopes'                    => false,
        'variables'                 => false,
        'parser'                    => false, // use default parser
        'refactor'                  => false, // use default refactor
        'compressors'               => array(
          // Order is important
          'CSSMin_Compressor_Remove_Comment' ,
          'CSSMin_Compressor_Remove_EmptyRuleset' ,
          'CSSMin_Compressor_Remove_EmptyAtMedia' ,
          'CSSMin_Compressor_Embed_Var' ,
          'CSSMin_Compressor_Embed_File_Url' ,
          'CSSMin_Compressor_Embed_File_AtImport' ,
        ),
        'contractors'               => array(
          // Order is important
          'CSSMin_Contractor_Float' ,
          'CSSMin_Contractor_Zero' ,
          'CSSMin_Contractor_Zeros' ,
          'CSSMin_Contractor_RGBColor' ,
          'CSSMin_Contractor_HexColor' ,
          'CSSMin_Contractor_Var' ,
          'CSSMin_Contractor_Url' ,
          'CSSMin_Contractor_AtImportUrl' ,
        ),
        'transformers'              => array(
          // Order is not important
          'CSSMin_Transformer_Simple_BorderRadius' ,
          'CSSMin_Transformer_Simple_BorderTopLeftRadius' ,
          'CSSMin_Transformer_Simple_BorderTopRightRadius' ,
          'CSSMin_Transformer_Simple_BorderBottomLeftRadius' ,
          'CSSMin_Transformer_Simple_BorderBottomRightRadius' ,
          'CSSMin_Transformer_Opacity' ,
        ),
        'bubblers'                  => array(
          // Order is important
          'CSSMin_Bubbler_AtVariables' ,
          'CSSMin_Bubbler_AtImport' ,
        ),
      );
	  
### Basic settings:

- **embed_basedir *[string]*:** Base directory for file embedding, used on `@import` rule and `url()` property value;
- **embed_maxsize *[integer]*:** Maximum size, in bytes, for embedded pictures on `url()` property value - above this limit, pictures will not be base64 encoded;
- **embed_iefix *[boolean]*:** Add a IE prefix for IE version that does not support base64 encoded `url()` property value;
- **scopes *[array]*:** Array of current scope of the CSS contents (`all`, `screen`, `print`…);
- **variables *[array]*:** Array of variables for the CSS contents - first level key is the variable scope, second level key is the variable name, then comes the variable value.

### Class settings:

Some settings use class names, in order to use them correctly there is two ways: the class exists or an autoloader can load it.

Before to manipulate these classes, you must know the group meaning:

- **Parsers:**
  A Parser class should be able to parse a CSS string to retrieve a collection of CSS tokens.
  Each Parser must implements `CSSMin_Parser_Interface`.
- **Refactors:**
  A Refactor class should be able to build a string from a collection of CSS tokens.
  Each Refactor must implements `CSSMin_Refactor_Interface`.
- **Compressors:**
  A Compressor class should manipulate a collection CSS tokens in order to remove and/or add CSS tokens.
  Each Compressor must implements `CSSMin_Compressor_Interface`.
- **Contractors:**
  A Contractor class should manipulate CSS tokens value, in order to reduce theirs length.
  Each Contractor must implements `CSSMin_Contractor_Interface`.
- **Transformers:**
  A Transformer class should manipulate a collection CSS tokens, in order to add CSS property tokens from another one - typically duplicate the property with each browser prefixes.
  Each Transformer must implements `CSSMin_Transformer_Interface`.
- **Bubblers:**
  A Bubbler class should manipulate a collection CSS tokens, in order to move some token from some place to another - typically on top of the contents.
  Each Bubbler must implements `CSSMin_Bubbler_Interface`.

If you understand what I want to mean, you should be able to manipulate these settings:

- **parser *[string]*:** Parser engine to use, default is `CSSMin_Parser_Standard`;
- **refactor *[string]*:** Refactor engine to use, default is `CSSMin_Refactor_Compressed` - note that a `CSSMin_Refactor_Standard` exists if you need some readable contents;
- **compressors *[array]*:** Array of compressors to call - order is important;
- **contractors *[array]*:** Array of contractors to call - order is important;
- **transformers *[array]*:** Array of transformers to call - order is not important;
- **bubblers *[array]*:** Array of bubblers to call - order is not important.


## Examples:

For now, there is only one example that is a real website CSS.