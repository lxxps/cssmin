<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Base class for CSS minifier
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: CSSMin.php 17 2013-04-05 09:30:06Z loops $
 */
class CSSMin extends CSSMin_Core
{
  /**
   * Current settings
   *
   * @var array
   *
   * @access protected
   */
  protected $_settings = array(
    'embed_basedir'             => false,
    'embed_maxsize'             => 3072,
    'embed_iefix'               => false,
    'parser'                    => false,
    'refactor'                  => false,
    'scopes'                    => false,
    'variables'                 => false,
    'compressors'               => array(
      // Order is important
      'CSSMin_Compressor_Remove_Comment' ,
//      'CSSMin_Compressor_Remove_UnknownRule' , // allow @-moz-document
      'CSSMin_Compressor_Remove_EmptyRuleset' ,
      'CSSMin_Compressor_Remove_EmptyAtMedia' ,
      'CSSMin_Compressor_Embed_Var' ,
      'CSSMin_Compressor_Embed_File_Url' ,
      'CSSMin_Compressor_Embed_File_AtImport' ,
    ),
    'contractors'               => array(
      // Order is important
      'CSSMin_Contractor_Float' ,
      'CSSMin_Contractor_Zero' ,
      'CSSMin_Contractor_Zeros' ,
      'CSSMin_Contractor_RGBColor' ,
      'CSSMin_Contractor_HexColor' ,
      'CSSMin_Contractor_Var' ,
      'CSSMin_Contractor_Url' ,
      'CSSMin_Contractor_AtImportUrl' ,
    ),
    'transformers'              => array(
      // Order is not important
      'CSSMin_Transformer_Simple_BorderRadius' ,
      'CSSMin_Transformer_Simple_BorderTopLeftRadius' ,
      'CSSMin_Transformer_Simple_BorderTopRightRadius' ,
      'CSSMin_Transformer_Simple_BorderBottomLeftRadius' ,
      'CSSMin_Transformer_Simple_BorderBottomRightRadius' ,
      'CSSMin_Transformer_Opacity' ,
//    'CSSMin_Transformer_WhiteSpace' , // Not usefull
      // Do not use it anymore since http://www.colorzilla.com/gradient-editor/ exists
//      'CSSMin_Transformer_Gradient' , 
//      'CSSMin_Transformer_BoxShadow' , // Experimental
//      'CSSMin_Transformer_TextShadow' , // Experimental
    ),
    'bubblers'                  => array(
      // Order is important
      'CSSMin_Bubbler_AtVariables' ,
      'CSSMin_Bubbler_AtImport' ,
    ),
  );

}
