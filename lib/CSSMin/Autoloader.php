<?php
/**
 * CSSMin - A CSS minifier with benefits
 * 
 * --
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Class to manage CSSMin plugin autoloading.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Autoloader.php 2 2010-09-03 15:13:57Z loops $
 */
class CSSMin_Autoloader
{
  /**
   * Current CSSMin_Autoloader instance.
   *
   * @var CSSMin_Autoloader Current instance
   * @access protected
   */
  protected static $_instance;
  
  /**
   * Root directory.
   *
   * @var string
   * @access protected
   */
  protected $_root;
  
  /**
   * Method to retrieve CSSMin_Autoloader instance.
   *
   * @param none
   * @return CSSMin_Autoloader Current instance
   * @access public
   * @static
   */ 
  public static function getInstance()
  {
  	if( empty(self::$_instance) )
  	{
  		self::$_instance = new self();
  	}
  	return self::$_instance;
  }
  
  /**
   * CSSMin_Autoloader constructor.
   * Protected constructor to force use of getInstance() static method.
   *
   * @param none
   * @return void
   * @access protected
   */
  protected function __construct()
  {
  	$this->_root = dirname( dirname( __FILE__ ) );
  	spl_autoload_register( array( $this , '_autoload' ) );
  }
  
  /**
   * Method to autoload class helping map.
   *
   * @param string $class Class name
   * @return void
   * @access protected
   */
  protected function _autoload( $class )
  {
    if( strpos( $class , 'CSSMin' ) === 0 && is_file( $file = $this->_root.DIRECTORY_SEPARATOR.str_replace( '_' , DIRECTORY_SEPARATOR , $class ).'.php' ) )
    {
      require $file;
    }
  }

}