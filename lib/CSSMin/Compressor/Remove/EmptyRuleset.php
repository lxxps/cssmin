<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Remove empty ruleset from tokens collection compressor.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: EmptyRuleset.php 4 2010-09-06 08:49:58Z loops $
 */
class CSSMin_Compressor_Remove_EmptyRuleset implements CSSMin_Compressor_Interface
{
  /**
   * Invoke the compression on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $p_pos = null;
    $flag = false;
    foreach( $tokens as $pos => $tok )
    {
      if( $tok->getType() === CSSMin_Token::TOKEN_SELECTORS )
      {
        $p_pos = $pos;
      }
      elseif( $p_pos !== null && $tok->getType() === CSSMin_Token::TOKEN_RULESET_STOP )
      {
        $flag = true;
        // Remove anything beetween this two token (especially comments)
        $tokens->remove( $p_pos , $pos );
      }
      // Do not consider comment
      if( $p_pos !== null && ( ! in_array( $tok->getType(), array( CSSMin_Token::TOKEN_SELECTORS , CSSMin_Token::TOKEN_COMMENT , CSSMin_Token::TOKEN_RULESET_START ) ) ) )
      {
        $p_pos = null;
      }
    }
    return $flag;
  }
}
