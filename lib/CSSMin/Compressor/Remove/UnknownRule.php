<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Remove unknown rules from tokens collection compressor.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: UnknownRule.php 4 2010-09-06 08:49:58Z loops $
 */
class CSSMin_Compressor_Remove_UnknownRule implements CSSMin_Compressor_Interface
{
  /**
   * Invoke the compression on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $p_pos = null;
    $flag = false;
    foreach( $tokens as $pos => $tok )
    {
      if( $tok->getType() === CSSMin_Token::TOKEN_AT_RULE && null === CSSMin_Parser_Standard::getAtRuleConstant( $tok->rule, 'RULE' ) )
      {
        $p_pos = $pos;
      }
      if( $p_pos !== null && $tok->getType() === CSSMin_Token::TOKEN_AT_UNKNOWN_STOP )
      {
        $flag = true;
        // Remove anything beetween this two token
        $tokens->remove( $p_pos , $pos );
      }
    }
    return $flag;
  }
}
