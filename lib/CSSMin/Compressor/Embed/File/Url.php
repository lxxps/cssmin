<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Embed url as base64 encoded.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Url.php 17 2013-04-05 09:30:06Z loops $
 */
class CSSMin_Compressor_Embed_File_Url extends CSSMin_Compressor_Embed_File_Base
{
  /**
   * Default maximum size of embed media (10Ko).
   *
   * @var int
   * @const
   */
  const DF_MAX_SIZE = 10240;

  /**
   * Save the max size value.
   *
   * @var boolean
   * @access protected
   */
  protected $_max_size;
  
  /**
   * Local IE fix flag.
   *
   * @var boolean
   * @access protected
   */
  protected $_ie_fix;

  /**
   * Construct the compressor.
   * Require current CSSMin_Core to read settings.
   *
   * @param  CSSMin_Core &$core
   *
   * @access public
   * @throws LogicException
   */
  public function __construct( CSSMin_Core &$core )
  {
    parent::__construct( $core );
    // Quick access to usefull configuration
    $this->_max_size = $core->getSetting( CSSMin_Core::SETTING_EMBED_MAXSIZE , self::DF_MAX_SIZE );
    $this->_ie_fix   = $core->getSetting( CSSMin_Core::SETTING_EMBED_IEFIX , false );
  }


  /**
   * Invoke the compression on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $flag = false;
    foreach( $tokens as $pos => $token )
    {
      // Escape "src" property (used on font) and proprety that allready have a '-' sign
      if( $token->getType() === CSSMin_Token::TOKEN_PROPERTY && $token->name !== 'src' && $token->name{0} !== CSSMin_Core::IE_PROPERTY_PREFIX && count( $token->urls ) )
      {
        // Initialize local flag
        // If we do not want IE fix, we can set it to true directly
        $l_flag = ! $this->_ie_fix;
        // Local copy of url
        $urls = $token->urls;
        // foreach url
        foreach( $urls as &$url )
        {
          if( $url['type'] === 'path' )
          {
            // Check if the file is an can be read
            if( ( $path = $this->_getPath( $url['path'] ) ) && ( $contents = @file_get_contents( $path ) ) )
            {
              // Does the file is an image
              if( $size = @getimagesize( $path ) )
              {
                // The file exists, check size
                if( strlen( $contents ) <= $this->_max_size )
                {
                  // At this point, we should introduce an hack for IE browser after the current proprety
                  // Note that we must do it for the first encoded url only
                  if( ! $l_flag )
                  {
                    $t = CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , CSSMin_Core::IE_PROPERTY_PREFIX.$token->name , $token->value );
                    $tokens->append( $t , $pos+1 );
                  }
                  
                  // Replace value of the token
                  $data = 'data:'.$size['mime'].';base64,'.base64_encode($contents);
                  // Alter value (they may be multiple url in value)
                  $token->value = str_replace( $url['str'] , 'url('.$data.')' , $token->value );
                  // Save new data
                  $url['str']  = 'url('.$data.')';
                  $url['type'] = 'data';
                  $url['content'] = $data;
                  $url['data'] = $data;
                  $url['path'] = null;
                  // Alter local flag
                  $l_flag = true;
                }
              }
            }
            else
            {
              // The file does not exists, remove data
              // Alter value
              $token->value = str_replace( $url['str'] , 'url()' , $token->value );
              // Save new data
              $url['str'] = 'url()';
              $url['content'] = '';
              $url['path'] = '';
              // Alter local flag
              $l_flag = true;
            }
          }
        }

        if( $l_flag )
        {
          // Alter urls informations
          $token->urls = $urls;
          // Alter main flag
          $flag = true;
        }
      }
    }
    return $flag;
  }
}
