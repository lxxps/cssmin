<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Base class for embed compressor.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @abstract
 * @subversion $Id: Base.php 13 2011-11-17 12:38:23Z loops $
 */
abstract class CSSMin_Compressor_Embed_File_Base implements CSSMin_Compressor_Interface, CSSMin_Compressor_Embed_Interface, CSSMin_Compressor_Embed_File_Interface
{
  /**
   * Save the base directory for memory usage.
   *
   * @var string
   * @access protected
   */
  protected $_base_dir;

  /**
   * Save the base directory is physical flag.
   *
   * @var boolean
   * @access protected
   */
  protected $_is_physical_base_dir;

  /**
   * Construct the compressor.
   * Require current CSSMin_Core to read settings.
   *
   * @param  CSSMin_Core &$core
   * 
   * @access public
   * @throws LogicException
   */
  public function __construct( CSSMin_Core &$core )
  {
    if( ! $core->getSetting( CSSMin_Core::SETTING_EMBED_BASEDIR ) )
    {
      throw new LogicException( sprintf( '%s need a base directory (CSSMin_Core::SETTING_EMBED_BASEDIR) to embed other media.' , get_class( $this ) ) );
    }
    // Quick access to usefull configuration
    $this->_base_dir = $core->getSetting( CSSMin_Core::SETTING_EMBED_BASEDIR );
    $this->_is_physical_base_dir = realpath( $this->_base_dir );
  }

  /**
   * Function that helps to retrieve path from base directory
   *
   * @param string $path
   * @return string
   * @access protected
   */
  protected function _getPath( $path )
  {
    if( $this->_is_physical_base_dir )
    {
      return $this->_getPhysicalPath( $path );
    }
    else
    {
      return $this->_getVirtualPath( $path );
    }
  }

  /**
   * Function that helps to retrieve virtual path from "embed_base_dir"
   *
   * @param string $path
   * @return string
   * @access protected
   */
  protected function _getVirtualPath( $path )
  {
    $c = parse_url( $path );
    $p = parse_url( $this->_base_dir );

    if( empty($c['host']) )
    {
      // Relative protocol, for now PHP does not 
      // parse relative protocol URL
      if( substr($path, 0, 2) == '//' )
      {
        $path = $p['scheme'].':'.$path;
      }
      
      // If the current path is root
      elseif( $path == '/' )
      {
        $path = $p['scheme'].'://'.$p['host'];
      }

      // If the current path starts from root '/'
      elseif( $path{0} == '/' )
      {
        $path = $p['scheme'].'://'.$p['host'].'/'.ltrim($path,'/');
      }

      // If the current path starts with '?'
      elseif( $path{0} == '?')
      {
        $path = $p['scheme'].'://'.$p['host'].'/'.ltrim($p['path'],'/').$path;
      }

      // If the current path starts with './'
      elseif( substr($path, 0, 2) == './' )
      {
        $path = $p['scheme'].'://'.$p['host'].'/'.ltrim($p['path'],'/').substr($path,2);
      }

      // If the current path starts with '..'
      elseif( substr($path, 0, 2) == '..' )
      {
        $tmp = explode( '/' , rtrim($this->_base_dir,'/') );
        array_pop( $tmp );
        $path = implode('/',$tmp).'/'.$path;
      }

      // Else
      else
      {
        $path = rtrim($this->_base_dir,'/').'/'.$path;
      }

      // Removed extra ../ in path
      if( strpos( $path , '..') !== false )
      {
        $tmp = array();
        foreach( explode('/',$path) as $part )
        {
          if( $part != '..' )
          {
            $tmp[] = $value;
          }
          else
          {
            array_pop($tmp);
          }
        }
        $path = implode('/',$tmp);
      }
    }

    return $path;
  }

  /**
   * Function that helps to retrieve physical path from "embed_base_dir"
   *
   * @param string $path
   * @return string
   *
   * @access protected
   */
  protected function _getPhysicalPath( $path )
  {
    // Prevent virtual path
    if( parse_url( $path , PHP_URL_HOST ) )
    {
      // We have a host, note that for now PHP does not 
      // parse relative protocol URL
      return $path;
    }
    if( substr($path, 0, 2) == '//' )
    {
      // We have a protocol relative path, use http scheme 
      // by default
      return 'http:'.$path;
    }
    
    // In other case, we can process the path as physical
    
    // Save current working directory
    $cwd = getcwd();
    // Change current working directory
    chdir( $this->_base_dir );
    // Apply realpath
    $path = realpath( $path );
    // Restore current working directory
    chdir( $cwd );
    // Return
    return $path;
  }
}
