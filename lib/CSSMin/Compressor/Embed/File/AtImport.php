<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Embed importes CSS to the current tokens collection.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: AtImport.php 8 2010-10-05 11:34:09Z loops $
 */
class CSSMin_Compressor_Embed_File_AtImport extends CSSMin_Compressor_Embed_File_Base
{
  /**
   * Clone of the current core.
   *
   * @var CSSMin_Core
   * @access protected
   */
  protected $_core;

  /**
   * Construct the compressor.
   * Require current CSSMin_Core to read settings and apply same compression to
   * each contents.
   *
   * @param  CSSMin_Core &$core
   * @return boolean
   *
   * @access public
   * @throws ConfigurationException
   */
  public function __construct( CSSMin_Core &$core )
  {
    parent::__construct( $core );
    // Clone the core do not alter current core
    $this->_core = clone $core;
    $this->_core->resetTokens();
  }
  
  /**
   * Invoke the compression on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $i_pos = null;
    $flag = false;
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_AT_RULE && $token->rule === CSSMin_Token::AT_RULE_IMPORT )
      {
        // Save position of the current import rule
        $i_pos = $pos;
      }
      elseif( $i_pos !== null && $token->getType() === CSSMin_Token::TOKEN_AT_IMPORT_SCOPES )
      {
        $flag = true;

        // Local copy of url
        $url = $token->url;
        // Check if the file is an can be read
        // Just to be safe, make a little check on url type
        if( $url['type'] === 'path' && ( $path = $this->_getPath( $url['path'] ) ) && ( $contents = @file_get_contents( $path ) ) )
        {
          // For now, we can only suppose that the file is a CSS one
          // Local clone
          $core = clone $this->_core;
          // Change base directory to the current one
          $core->setSetting( CSSMin_Core::SETTING_EMBED_BASEDIR , dirname( $path ) );
          // Set current scopes
          $core->setSetting( CSSMin_Core::SETTING_SCOPES , $token->scopes ? $token->scopes : array( 'all' ) );
          // Embed collection (just compress)
          $embed_tokens = $core->parse( $contents )->compress()->getTokens();

          // Foreach url() property value, append current base_dir diff
          // Note that url in IE filter are not modified
          if( ( $diff = pathinfo( $url['path'] , PATHINFO_DIRNAME ) ) !== '.' )
          {
            foreach( $embed_tokens as $embed_pos => $embed_token )
            {
              if( ( $embed_token->getType() === CSSMin_Token::TOKEN_PROPERTY || $embed_token->getType() === CSSMin_Token::TOKEN_VARIABLE ) && count( $embed_token->urls ) )
              {
                // Initialize local flag
                $flag = false;
                // Local copy of url
                $urls = $embed_token->urls;
                // Foreach url
                foreach( $urls as &$url )
                {
                  if( $url['type'] === 'path' && ( ! preg_match( '~^((https?://)|/|data:)~' , pathinfo( $url['path'] , PATHINFO_DIRNAME ) ) ) )
                  {
                    $data = $diff.'/'.$url['path'];
                    // Alter value (they may be multiple url in value)
                    // We attemp to not change the original aspect (quotes, spaces...)
                    $embed_token->value = str_replace( $url['str'] , str_replace( $url['path'] , $data , $url['str'] ) , $embed_token->value );
                    // Save new data
                    $url['str'] = str_replace( $url['path'] , $data , $url['str'] );
                    $url['content'] = str_replace( $url['path'] , $data , $url['content'] );
                    $url['path'] = $data;
                    // Alter local flag
                    $flag = true;
                  }
                }
                
                if( $flag )
                {
                  // Alter urls informations
                  $embed_token->urls = $urls;
                }
              }
            }            
          }

          // Is there any scope to the import rule
          if( $token->scopes )
          {
            // Extract @media and @variable rules to append them before @import
            $m_pos = null;
            $v_pos = null;
            foreach( $embed_tokens as $embed_pos => $embed_token )
            {
              if( $embed_token->getType() === CSSMin_Token::TOKEN_AT_RULE && $embed_token->rule === CSSMin_Token::AT_RULE_MEDIA )
              {
                $m_pos = $embed_pos;
              }
              if( $embed_token->getType() === CSSMin_Token::TOKEN_AT_RULE && $embed_token->rule === CSSMin_Token::AT_RULE_VARIABLES )
              {
                $v_pos = $embed_pos;
              }
              if( $m_pos !== null && $embed_tokens->getType() === CSSMin_Token::TOKEN_AT_MEDIA_STOP )
              {
                // Move tokens before @import rule
                $tokens->embed( $tmp = $embed_tokens->extract( $m_pos , $embed_pos ) , $i_pos - 1 );
                // Alter insert position
                $i_pos += count( $tmp );
                $m_pos = null;
              }
              if( $v_pos !== null && $embed_tokens->getType() === CSSMin_Token::TOKEN_SCOPES )
              {
                // Consider that variables has the same scope than current @import scopes
                $embed_tokens->scopes = array_unique( array_merge( $embed_tokens->scopes , $token->scopes ) );
              }
              if( $v_pos !== null && $embed_tokens->getType() === CSSMin_Token::TOKEN_RULESET_STOP )
              {
                // Move tokens before @import rule
                $tokens->embed( $tmp  = $embed_tokens->extract( $v_pos , $embed_pos ) , $i_pos - 1 );
                // Alter insert position
                $i_pos += count( $tmp );
                $v_pos = null;
              }
            }
            
            // Remove everything beetween @import token and import scope token (especially comment)
            $tokens->remove( $i_pos , $pos );

            // Add @media rule (note increment after usage)
            $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_RULE , CSSMin_Token::AT_RULE_MEDIA ) , $i_pos++ );
            // Add scopes (note increment after usage)
            $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_SCOPES , $token->scopes ) , $i_pos++ );
            // Add @media start (note increment after usage)
            $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_MEDIA_START ) , $i_pos++ );
            // Embed tokens (note increment after usage)
            $tokens->embed( $embed_tokens , $i_pos++ );
            // Alter insert position
            $i_pos += count( $embed_tokens );
            // Add @media stop  (note increment after usage)
            $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_MEDIA_STOP ) , $i_pos++ );
          }
          else
          {
            // Remove everything beetween @import token and import scope token (especially comment)
            $tokens->remove( $i_pos , $pos );
            // Embed import at import position
            $tokens->embed( $embed_tokens , $i_pos );
          }
        }
        else
        {
          // The file does not exists
          // Remove everything beetween @import token and import scope token (especially comment)
          $tokens->remove( $i_pos , $pos );
        }
        $i_pos = null;
      }
      elseif( $i_pos !== null && $token->getType() !== CSSMin_Token::TOKEN_COMMENT )
      {
        $i_pos = null;
      }
    }
    return $flag;
  }
}
