<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Embed variables.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Var.php 4 2010-09-06 08:49:58Z loops $
 */
class CSSMin_Compressor_Embed_Var implements CSSMin_Compressor_Interface, CSSMin_Compressor_Embed_Interface
{
  /**
   * Variables list by media type
   * 
   * @var array
   * @access protected
   */
  protected $_vars = array();

  /**
   * Current core.
   *
   * @var CSSMin_Core
   * @access protected
   */
  protected $_core;
  
  /**
   * Current core scopes.
   *
   * @var array
   * @access protected
   */
  protected $_scopes;

  /**
   * Construct the compressor.
   * Require current CSSMin_Core to read settings and append variables.
   *
   * @param  CSSMin_Core &$core
   * @return boolean
   * @access public
   */
  public function __construct( CSSMin_Core &$core )
  {
    // Quick access to usefull configuration
    $this->_scopes = $core->getSetting( CSSMin_Core::SETTING_SCOPES , array('all') );
    $this->_vars   = $core->getSetting( CSSMin_Core::SETTING_VARIABLES , array() );
    // Get the core to alter it
    $this->_core = $core;
  }

  /**
   * Invoke the compression on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $flag = false;

    // At first fetch all variables in the collection
    $v_pos = null;
    $c_scopes = $this->_scopes;
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_AT_RULE && $token->rule === CSSMin_Token::AT_RULE_VARIABLES )
      {
        $v_pos = $pos;
      }
      if( $v_pos !== null && $token->getType() === CSSMin_Token::TOKEN_SCOPES )
      {
        $c_scopes = $token->scopes;
      }
      if( $v_pos !== null && $token->getType() === CSSMin_Token::TOKEN_VARIABLE )
      {
        $this->add( $token->name , $token->value , $c_scopes );
      }
      if( $v_pos !== null && $token->getType() === CSSMin_Token::TOKEN_RULESET_STOP )
      {
        // Remove everything beetween @variables token until this token
        $tokens->remove( $v_pos , $pos );
        $v_pos = null;
        $c_scopes = $this->_scopes;
      }
    }

    // Append variables to current core
    $this->_core->setSetting( CSSMin_Core::SETTING_VARIABLES , $this->_vars );

    // Now, apply variables
    $m_found = false;
    $c_scopes = $this->_scopes;
    $matches = array();
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_AT_RULE && $token->rule === CSSMin_Token::AT_RULE_MEDIA )
      {
        $m_found = true;
      }
      if( $m_found && $token->getType() === CSSMin_Token::TOKEN_SCOPES )
      {
        $c_scopes = $token->scopes;
      }
      if( $m_found && $token->getType() === CSSMin_Token::TOKEN_AT_MEDIA_STOP )
      {
        $c_scopes = $this->_scopes;
        $m_found = false;
      }
      if( $token->getType() === CSSMin_Token::TOKEN_PROPERTY && preg_match_all( '~(^|\\s)var\\(([^\\)]+)\\)~U' , $token->value , $matches , PREG_SET_ORDER ) )
      {
        $flag = true;
        for( $i = 0, $imax = count($matches); $i < $imax; $i++ )
        {
          if( $matches[$i][0]{0} === 'v' )
          {
            // If true, we can only replace the begin of the value
            $token->value = substr_replace( $token->value , $this->get( $matches[$i][2] , $c_scopes ) , 0 , strlen($matches[$i][0]) );
          }
          else
          {
            // Replace everyting
            $token->value = str_replace( $matches[$i][0] , ' '.$this->get( $matches[$i][2] , $c_scopes ) , $token->value );
          }
        }
      }
    }

  }

  /**
   * Add a new variable
   *
   * @param string $name
   * @param string $value
   * @param array  $scopes
   * @return CSSMin_Compressor_Embed_Var
   *
   * @access public
   */
  public function add( $name , $value , array $scopes = array() )
  {
    if( ! $scopes ) $scopes = array('all');
    foreach( $scopes as $scope ) $this->_vars[$scope][$name] = $value;
    return $this;
  }

  /**
   * Get a variable by its scope
   *
   * @param string $name
   * @param array  $scopes
   * @return string
   *
   * @access public
   * @throws InvalidArgumentException
   */
  public function get( $name , array $scopes = array() )
  {
    // Automatically add scope "all"
    $scopes = array_merge( $scopes , array('all') );
    foreach( $scopes as $scope )
    {
      if( isset( $this->_vars[$scope][$name] ) ) return $this->_vars[$scope][$name];
    }
    throw new InvalidArgumentException( sprintf( 'Unable to found variable "%s" for scopes "%s".' , $name , var_export( $scopes , 1 ) ) );
  }
  
}