<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Token collection class.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Collection.php 4 2010-09-06 08:49:58Z loops $
 */
class CSSMin_Token_Collection implements Iterator, Countable
{
  /**
   * Tokens list
   *
   * @var array
   * @access protected
   */
  protected $_tokens = array();

  /**
   * Current position for iterator
   *
   * @var integer
   * @access protected
   */
  protected $_position = 0;

  /**
   * Constructor
   *
   * @param array Tokens array
   * 
   * @access public
   */
  public function __construct( array $tokens = array() )
  {
    foreach( $tokens as $token )
    {
      $this->add( $token );
    }
  }

  /**
   * Magic clone function
   *
   * @param none
   *
   * @access public
   */
  public function __clone()
  {
    // Clone each tokens
    $tokens = array();
    foreach( $this as $token )
    {
      $tokens[] = clone $token;
    }
    $this->_tokens = $tokens;
  }

  /**
   * Add a token to the list
   *
   * @param CSSMin_Token $token
   * @return CSSMin_Token_Collection
   *
   * @access public
   */
  public function add( CSSMin_Token $token )
  {
    $this->_tokens[] = $token;
    // The position does not have to change
    return $this;
  }

  /**
   * Remove a token from the list.
   * If second argument is provided, remove a range of tokens.
   *
   * @param  integer $pos
   * @param  integer $to
   * @return CSSMin_Token_Collection
   *
   * @access public
   * @throws OutOfRangeException
   */
  public function remove( $pos , $to = null )
  {
    if( $to !== null )
    {
      // Remove range
      for( $i = $pos, $imax = count($this); $i <= $to && $i < $imax; $i++ )
      {
        // We use $pos because tokens are rearrange each time
        $this->remove( $pos );
      }
    }
    else
    {
      if( ! isset( $this->_tokens[$pos] ) )
      {
        throw new OutOfRangeException( sprintf( 'No token at position "%d".' , $pos ) );
      }
      unset( $this->_tokens[$pos] );
      // Re-order tokens
      $this->_tokens = array_values( $this->_tokens );
      // Decrement position
      --$this->_position;
    }
    return $this;
  }

  /**
   * Appends one token to the given position.
   *
   * @param  integer $pos
   * @param  CSSMin_Token $token
   * @return CSSMin_Token_Collection
   *
   * @access public
   */
  public function append( CSSMin_Token $token , $pos = 0 )
  {
    array_splice( $this->_tokens , $pos, 0 , array( $token ) );
    // Increment position
    ++$this->_position;
    return $this;
  }

  /**
   * Embed a collection of tokens to a certain position
   *
   * @param  CSSMin_Token_Collection $collection
   * @param  integer $pos
   * @return CSSMin_Token_Collection
   *
   * @access public
   */
  public function embed( CSSMin_Token_Collection $collection , $pos = 0 )
  {
    // Clone collection
    $collection = clone $collection;
    $tokens = array();
    foreach( $collection as $tok ) $tokens[] = $tok;
    // Before splice, retrieve the end key
    array_splice( $this->_tokens , $pos , 0 , $tokens );
    // Increment position
    $this->_position += count($collection);
    return $this;
  }

  /**
   * Extract a portion of the collection
   *
   * @param  integer $from
   * @param  integer $to
   * @return CSSMin_Token_Collection
   *
   * @access public
   */
  public function extract( $from , $to )
  {
    $tokens = new CSSMin_Token_Collection();
    // Add tokens to the new collection
    for( $i = $from, $imax = count($this); $i <= $to && $i < $imax; $i++ )
    {
      $tokens->add( $this->_tokens[$i] );
    }
    // Remove tokens from the current collection
    $this->remove( $from , $to );
    return $tokens;
  }

  /* Iterator interfaced methods */
  public function current(){ return $this->_tokens[$this->_position]; }
  public function key(){ return $this->_position; }
  public function next(){ ++$this->_position; }
  public function rewind(){ $this->_tokens = array_values( $this->_tokens ); $this->_position = 0; }
  public function valid() { return isset($this->_tokens[$this->_position]); }
  /* Countable interfaced methods */
  public function count(){ return count( $this->_tokens ); }

}
