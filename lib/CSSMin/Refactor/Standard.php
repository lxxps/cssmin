<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Refactor class for CSS min.
 * Reformat CSS as standard view.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Standard.php 10 2011-08-08 15:07:08Z loops $
 */
class CSSMin_Refactor_Standard implements CSSMin_Refactor_Interface
{
  /**
   * Compile tokens to render CSS.
   *
   * @param  &CSSMin_Token_Collection $tokens
   * @return string
   *
   * @access public
   * @throws UnexpectedValueException
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $str = '/**'."\n"
         . ' * @generator  CSSMin 3.0.0.$Rev: 10 $'."\n"
         . ' * @author     Pierrot Evrard <pierrotevrard at gmail.com>'."\n"
         . ' * @date       '.date('r')."\n"
         . ' */'."\n";
    // Fake token for convenience
    $p_tok = CSSMin_Token::create( 'Comment' , '/* Fake token */' );
    foreach( $tokens as $tok )
    {
      switch( $tok->getType() )
      {
        case CSSMin_Token::TOKEN_COMMENT :
        {
          $str .= $tok->comment."\n";
        }
        break;
        case CSSMin_Token::TOKEN_AT_RULE :
        {
          $str .= '@'.$tok->rule;
        }
        break;
        case CSSMin_Token::TOKEN_SCOPES :
        {
          if( $tok->scopes ) $str .= ' '.implode(' , ',$tok->scopes);
        }
        break;
        case CSSMin_Token::TOKEN_SELECTORS :
        {
          $str .= implode(','."\n",$tok->selectors).' ';
        }
        break;
        case CSSMin_Token::TOKEN_PROPERTY :
        case CSSMin_Token::TOKEN_VARIABLE :
        {
          $str .= $tok->name.': '.$tok->value.';'."\n";
        }
        break;
        case CSSMin_Token::TOKEN_AT_IMPORT_SCOPES :
        {
          $str .= ' '.$tok->url['str'];
          if( $tok->scopes ) $str .= ' '.implode(',',$tok->scopes);
          $str .= ';'."\n";
        }
        break;
        case CSSMin_Token::TOKEN_RULESET_START :
        case CSSMin_Token::TOKEN_AT_MEDIA_START :
        case CSSMin_Token::TOKEN_AT_UNKNOWN_START :
        {
          $str .= '{'."\n";
        }
        break;
        case CSSMin_Token::TOKEN_RULESET_STOP :
        case CSSMin_Token::TOKEN_AT_MEDIA_STOP :
        case CSSMin_Token::TOKEN_AT_UNKNOWN_STOP :
        {
          $str .= '}'."\n";
        }
        break;
        default :
        {
          throw new UnexpectedValueException( sprintf('Unknown token "%s".' , $tok->getType() ) );
        }
      }
      $p_tok = $tok;
    }
    return $str;
  }
}
