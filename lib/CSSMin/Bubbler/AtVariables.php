<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Bubbling variables to the top of the collection.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: AtVariables.php 2 2010-09-03 15:13:57Z loops $
 */
class CSSMin_Bubbler_AtVariables implements CSSMin_Bubbler_Interface
{
  /**
   * Invoke the bubbling on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $flag = false;
    $v_pos = null;
    $top = 0;
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_AT_RULE && $token->rule === CSSMin_Token::AT_RULE_VARIABLES )
      {
        $v_pos = $pos;
      }
      if( $v_pos !== null && $token->getType() === CSSMin_Token::TOKEN_RULESET_STOP )
      {
        $flag = true;
        // Extract @variables declaration
        $tokens->embed( $tmp = $tokens->extract( $v_pos , $pos ) , $top );
        // Alter top position
        $top += count( $tmp );
        $v_pos = null;
      }
    }
    return $flag;
  }
}
