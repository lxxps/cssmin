<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Core class for CSS minifier
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Core.php 17 2013-04-05 09:30:06Z loops $
 * @abstract
 */
abstract class CSSMin_Core
{
  /**
   * Constant used as property prefix for IE hack
   *
   * @var string
   * @const
   */
  const IE_PROPERTY_PREFIX = '*';

  /**
   * Embed base directory setting name
   *
   * @var string
   * @const
   */
  const SETTING_EMBED_BASEDIR = 'embed_basedir';

  /**
   * Embed max size setting name
   *
   * @var string
   * @const
   */
  const SETTING_EMBED_MAXSIZE = 'embed_maxsize';

  /**
   * Do IE fix when embed
   *
   * @var string
   * @const
   */
  const SETTING_EMBED_IEFIX = 'embed_iefix';

  /**
   * Scopes setting name
   *
   * @var string
   * @const
   */
  const SETTING_SCOPES = 'scopes';
  
  /**
   * Variables setting name
   *
   * @var string
   * @const
   */
  const SETTING_VARIABLES = 'variables';
  
  /**
   * Compressors setting name
   *
   * @var string
   * @const
   */
  const SETTING_CONTRACTORS = 'contractors';
  
  /**
   * Compressors setting name
   *
   * @var string
   * @const
   */
  const SETTING_COMPRESSORS = 'compressors';
  
  /**
   * Transformers setting name
   *
   * @var string
   * @const
   */
  const SETTING_TRANSFORMERS = 'transformers';
  
  /**
   * Bubblers setting name
   *
   * @var string
   * @const
   */
  const SETTING_BUBBLERS = 'bubblers';
  
  /**
   * Parser setting name
   *
   * @var string
   * @const
   */
  const SETTING_PARSER = 'parser';
  
  /**
   * Refactor setting name
   *
   * @var string
   * @const
   */
  const SETTING_REFACTOR = 'refactor';

  /**
   * Trim characters list
   *
   * @var string
   * @const
   */
  const TRIM_CHARS = " \t\n\r\0\x0B";

  /**
   * Trim utility function.
   *
   * @param string $value
   * @param string $chars
   * @return string
   *
   * @access public
   * @static
   */
  public static function trim( $value , $chars = '' )
  {
    return trim( $value , self::TRIM_CHARS.$chars);
  }

  /**
   * Minify CSS from a file or a string.
   *
   * @param string $file
   * @param array $settings
   * @return string
   *
   * @access public
   * @static
   * @throws InvalidArgumentException
   */
  public static function minify( $css , array $settings = array() )
  {
    if( parse_url( $css ) )
    {
      return self::minifyFile( $css , $settings );
    }
    else
    {
      return self::minifyString( $css , $settings );
    }
  }

  /**
   * Minify CSS from a file.
   *
   * @param string $file
   * @param array $settings
   * @return string
   *
   * @access public
   * @static
   * @throws InvalidArgumentException
   */
  public static function minifyFile( $file , array $settings = array() )
  {
    // Check if the file exists
    if( ! $contents = @file_get_contents( $file ) )
    {
      throw new InvalidArgumentException( sprintf( 'Unable to get content of file "%s" does not exists.' , $file ) );
    }
    // Automatically set base directory
    $settings[self::SETTING_EMBED_BASEDIR] = dirname( $file );
    return self::minifyString( $contents , $settings );
  }

  /**
   * Minify CSS from a file.
   *
   * @param string $file
   * @param array $settings
   * @return string
   *
   * @access public
   * @static
   * @throws InvalidArgumentException
   */
  public static function minifyString( $contents , array $settings = array() )
  {
    if( ! $contents ) return '';
    // Automatically set base directory
    $min = new CSSMin( $settings );
    return $min->__invoke( $contents );
  }

  /**
   * Current settings
   *
   * @var array
   *
   * @access protected
   */
  protected $_settings = array(
    'embed_basedir'             => false,
    'embed_maxsize'             => false,
    'embed_iefix'               => false,
    'parser'                    => false,
    'refactor'                  => false,
    'scopes'                    => false,
    'variables'                 => false,
    'compressors'               => array(
      // Order is important
      'CSSMin_Compressor_Remove_Comment' ,
      'CSSMin_Compressor_Remove_UnknownRule' ,
      'CSSMin_Compressor_Remove_EmptyRuleset' ,
      'CSSMin_Compressor_Remove_EmptyAtMedia' ,
      'CSSMin_Compressor_Embed_Var' ,
      'CSSMin_Compressor_Embed_File_Url' ,
      'CSSMin_Compressor_Embed_File_AtImport' ,
    ),
    'contractors'               => array(
      // Order is important
      'CSSMin_Contractor_Float' ,
      'CSSMin_Contractor_Zero' ,
      'CSSMin_Contractor_Zeros' ,
      'CSSMin_Contractor_RGBColor' ,
      'CSSMin_Contractor_HexColor' ,
      'CSSMin_Contractor_Var' ,
      'CSSMin_Contractor_Url' ,
      'CSSMin_Contractor_AtImportUrl' ,
    ),
    'transformers'              => array(
      // Order is not important
      'CSSMin_Transformer_Simple_BorderRadius' ,
      'CSSMin_Transformer_Simple_BorderTopLeftRadius' ,
      'CSSMin_Transformer_Simple_BorderTopRightRadius' ,
      'CSSMin_Transformer_Simple_BorderBottomLeftRadius' ,
      'CSSMin_Transformer_Simple_BorderBottomRightRadius' ,
      'CSSMin_Transformer_Opacity' ,
      //'CSSMin_Transformer_WhiteSpace' , // Not usefull
      'CSSMin_Transformer_Gradient' , // Does not work on Opera
      'CSSMin_Transformer_BoxShadow' , // Experimental
      'CSSMin_Transformer_TextShadow' , // Experimental
    ),
    'bubblers'                  => array(
      // Order is important
      'CSSMin_Bubbler_AtVariables' ,
      'CSSMin_Bubbler_AtImport' ,
    ),
  );

  /**
   * Tokens collection
   *
   * @var CSSMin_Token_Collection
   * @access protected
   */
  protected $_tokens;

  /**
   * Create a CSSMin instance
   *
   * @param string $css
   * @param array  $settings
   * @return string
   *
   * @access public
   */
  public function __construct( array $settings = array() )
  {
    // Note usage of + instead of array_merge() to be able to overwrite true values
    foreach( $settings as $setting => $value )
    {
      $this->setSetting( $setting , $value );
    }
  }

  /**
   * Magic function to clone current instance.
   * Clone tokens and reset classes.
   *
   * @param none
   *
   * @access public
   */
  public function __clone()
  {
    if( $this->_tokens !== null )
    {
      $this->_tokens = clone $this->_tokens;
    }
    // Replace class by string to recall constructors
    foreach( array( 'parser' , 'refactor' , 'contractors' , 'compressors' , 'transformers' , 'bubblers' ) as $key )
    {
      if( is_array( $this->_settings[$key] ) )
      {
        foreach( $this->_settings[$key] as $i => $class )
        {
          if( is_object($class) )
          {
            $this->_settings[$key][$i] = get_class( $class );
          }
        }
      }
      else
      {
        if( is_object($this->_settings[$key]) )
        {
          $this->_settings[$key] = get_class( $this->_settings[$key] );
        }
      }
    }
  }

  /**
   * Read setting
   *
   * @param string $setting
   * @param mixed  $df
   * @return mixed
   *
   * @access public
   * @throws InvalidArgumentException
   */
  public function getSetting( $setting = null , $df = null )
  {
    if( $setting === null )
    {
      return $this->_settings;
    }
    $setting = (string)$setting;
    if( isset($this->_settings[$setting]) )
    {
      if( $this->_settings[$setting] === false )
      {
        return $df;
      }
      else
      {
        return $this->_settings[$setting];
      }
    }
    throw new InvalidArgumentException( sprintf( 'Setting "%s" does not exist.' , $setting ) );
  }

  /**
   * Set setting
   *
   * @param string $setting
   * @param mixed  $value
   * @return CSSMin_Core
   *
   * @access public
   * @throws InvalidArgumentException
   */
  public function setSetting( $setting , $value )
  {
    $setting = (string)$setting;
    if( ! isset($this->_settings[$setting]) )
    {
      throw new InvalidArgumentException( sprintf( 'Setting "%s" does not exist.' , $setting ) );
    }
    $this->_settings[$setting] = $value;
    return $this;
  }

  /**
   * Invoke minification on CSS string.
   *
   * @param string $css
   * @return string
   *
   * @access public
   */
  public function __invoke( $css )
  {
    return $this->parse( $css )->compress()->contract()->transform()->bubble()->refactor();
  }

  /**
   * Parse CSS string.
   *
   * @param string $css
   * @return CSSMin_Core
   *
   * @access public
   */
  public function parse( $css )
  {
    $parser = $this->getSetting( self::SETTING_PARSER , 'CSSMin_Parser_Standard' );
    if( ! in_array( 'CSSMin_Parser_Interface' , class_implements( $parser , true ) , true ) )
    {
      throw new UnexpectedValueException( sprintf( 'Parser %s must implements CSSMin_Parser_Interface.' , $refactor ) );
    }
    $parser = new $parser();
    $this->_tokens = $parser->__invoke( $css );
    
//    print '<h1>Parsed:</h1>';
//    print '<pre>';
//    print_r( $this->_tokens );
//    print '</pre>';

    return $this;
  }

  /**
   * Apply contractors.
   *
   * @param none
   * @return CSSMin_Core
   *
   * @access public
   * @throws UnexpectedValueException
   */
  public function contract()
  {
    // Retrieve contractors
    $contractors = $this->getSetting( self::SETTING_CONTRACTORS , array() );

    // Check contractors (do not use reference)
    foreach( $contractors as $key => $contractor )
    {
      if( is_string( $contractor ) )
      {
        if( ! in_array( 'CSSMin_Contractor_Interface' , class_implements( $contractor , true ) , true ) )
        {
          throw new UnexpectedValueException( sprintf( 'Contractor %s must implements CSSMin_Contractor_Interface.' , $contractor ) );
        }
        $contractors[$key] = new $contractor();
      }
      elseif( ! $contractor instanceof CSSMin_Contractor_Interface )
      {
        throw new UnexpectedValueException( sprintf( 'Contractor %s must implements CSSMin_Contractor_Interface.' , get_class( $contractor ) ) );
      }
    }

    // Append new contractors to not process creation twice
    $this->setSetting( self::SETTING_CONTRACTORS , $contractors );

    // Apply contractors
    foreach( $contractors as $contractor )
    {
      foreach( $this->_tokens as $token )
      {
        $contractor->detect( $token ) && $contractor->__invoke( $token );
      }
    }

//    print '<h1>Contracted:</h1>';
//    print '<pre>';
//    print_r( $this->_tokens );
//    print '</pre>';

    return $this;
  }

  /**
   * Apply compressors.
   *
   * @param none
   * @return CSSMin_Core
   *
   * @access public
   * @throws UnexpectedValueException 
   */
  public function compress()
  {
    // Retrieve compressors
    $compressors = $this->getSetting( self::SETTING_COMPRESSORS , array() );
    
    // Check compressors (do not use reference)
    foreach( $compressors as $key => $compressor )
    {
      if( is_string( $compressor ) )
      {
        if( ! in_array( 'CSSMin_Compressor_Interface' , $implements = class_implements( $compressor , true ) , true ) )
        {
          throw new UnexpectedValueException ( sprintf( 'Compressor %s must implements CSSMin_Compressor_Interface.' , $compressor ) );
        }
        // Special check for embed compressors
        if( in_array( 'CSSMin_Compressor_Embed_Interface' , $implements , true ) )
        {
          // Invoke embed file compressor only if embed base directory is defined
          if( ( ! in_array( 'CSSMin_Compressor_Embed_File_Interface' , $implements , true ) ) || $this->getSetting( self::SETTING_EMBED_BASEDIR ) )
          {
            $compressors[$key] = new $compressor( $this );
          }
        }
        else
        {
          $compressors[$key] = new $compressor();
        }
      }
      elseif( ! $compressor instanceof CSSMin_Compressor_Interface )
      {
        throw new UnexpectedValueException( sprintf( 'Compressor %s must implements CSSMin_Compressor_Interface.' , get_class( $compressor ) ) );
      }
    }
    
    // Append new compressors to not process creation twice
    $this->setSetting( self::SETTING_COMPRESSORS , $compressors );

    // Apply contractors
    foreach( $compressors as $compressor )
    {
      // Invoke embed file compressor only if embed base directory is defined
      if( ! is_string( $compressor ) )
      {
        $compressor->__invoke( $this->_tokens );
      }
    }

//    print '<h1>Compressed:</h1>';
//    print '<pre>';
//    print_r( $this->_tokens );
//    print '</pre>';

    return $this;
  }

  /**
   * Apply transformers.
   *
   * @param none
   * @return CSSMin_Core
   *
   * @access public
   * @throws UnexpectedValueException
   */
  public function transform()
  {
    // Retrieve transformers
    $transformers = $this->getSetting( self::SETTING_TRANSFORMERS , array() );

    // Check transformers (do not use reference)
    foreach( $transformers as $key => $transformer )
    {
      if( is_string( $transformer ) )
      {
        if( ! in_array( 'CSSMin_Transformer_Interface' , class_implements( $transformer , true ) , true ) )
        {
          throw new UnexpectedValueException( sprintf( 'Transformer %s must implements CSSMin_Transformer_Interface.' , $transformer ) );
        }
        $transformers[$key] = new $transformer();
      }
      elseif( ! $transformer instanceof CSSMin_Transformer_Interface )
      {
        throw new UnexpectedValueException( sprintf( 'Transformer %s must implements CSSMin_Transformer_Interface.' , get_class( $transformer ) ) );
      }
    }

    // Append new transformers to not process creation twice
    $this->setSetting( self::SETTING_TRANSFORMERS , $transformers );

    // Apply contractors
    foreach( $transformers as $transformer )
    {
      $transformer->__invoke( $this->_tokens );
    }

//    print '<h1>Transformed:</h1>';
//    print '<pre>';
//    print_r( $this->_tokens );
//    print '</pre>';

    return $this;
  }

  /**
   * Apply bubblers.
   *
   * @param none
   * @return CSSMin_Core
   *
   * @access public
   * @throws UnexpectedValueException
   */
  public function bubble()
  {
    // Retrieve bubblers
    $bubblers = $this->getSetting( self::SETTING_BUBBLERS , array() );

    // Check bubblers (do not use reference)
    foreach( $bubblers as $key => $bubbler )
    {
      if( is_string( $bubbler ) )
      {
        if( ! in_array( 'CSSMin_Bubbler_Interface' , class_implements( $bubbler , true ) , true ) )
        {
          throw new UnexpectedValueException( sprintf( 'Bubbler %s must implements CSSMin_Bubbler_Interface.' , $bubbler ) );
        }
        $bubblers[$key] = new $bubbler();
      }
      elseif( ! $bubbler instanceof CSSMin_Bubbler_Interface )
      {
        throw new UnexpectedValueException( sprintf( 'Bubbler %s must implements CSSMin_Bubbler_Interface.' , get_class( $bubbler ) ) );
      }
    }

    // Append new bubblers to not process creation twice
    $this->setSetting( self::SETTING_BUBBLERS , $bubblers );

    // Apply contractors
    foreach( $bubblers as $bubbler )
    {
      $bubbler->__invoke( $this->_tokens );
    }
    
//    print '<h1>Bubbled:</h1>';
//    print '<pre>';
//    print_r( $this->_tokens );
//    print '</pre>';

    return $this;
  }

  /**
   * Refactor tokens.
   *
   * @param none
   * @return string
   *
   * @access public
   */
  public function refactor()
  {
    // Retrieve refactor class
    $refactor = $this->getSetting( self::SETTING_REFACTOR , 'CSSMin_Refactor_Compressed' );
    if( ! in_array( 'CSSMin_Refactor_Interface' , class_implements( $refactor , true ) , true ) )
    {
      throw new UnexpectedValueException( sprintf( 'Refactor %s must implements CSSMin_Refactor_Interface.' , $refactor ) );
    }
    $refactor = new $refactor();
    return $refactor->__invoke( $this->_tokens );
  }

  /**
   * Retrieve tokens.
   *
   * @param none
   * @return CSSMin_Token_Collection
   *
   * @access public
   */
  public function getTokens()
  {
    return $this->_tokens;
  }

  /**
   * Reset tokens.
   *
   * @param none
   * @return CSSMin_Core
   *
   * @access public
   */
  public function resetTokens()
  {
    $this->_tokens = null;
    return $this;
  }

}