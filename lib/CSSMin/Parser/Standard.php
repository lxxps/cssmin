<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Parser class for CSS min.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Standard.php 18 2013-07-05 14:58:47Z loops $
 */
class CSSMin_Parser_Standard implements CSSMin_Parser_Interface
{
  /**
   * Current state can have comment
   *
   * @var integer
   * @const
   */
  const STATEFLAG_COMMENT = 1;

  /**
   * Current state can have @rules
   *
   * @var integer
   * @const
   */
  const STATEFLAG_AT_RULE = 2;

  /**
   * Current state can have @variables rule
   * Value: 4 + 2 (STATEFLAG_AT_RULE)
   *
   * @var integer
   * @const
   */
  const STATEFLAG_AT_RULE_VARIABLES = 6;

  /**
   * Current state can have @import rule
   * Value: 8 + 2 (STATEFLAG_AT_RULE)
   *
   * @var integer
   * @const
   */
  const STATEFLAG_AT_RULE_IMPORT = 10;

  /**
   * Current state can have scope
   *
   * @var integer
   * @const
   */
  const STATEFLAG_SCOPE = 16;

  /**
   * Current state can have ruleset
   *
   * @var integer
   * @const
   */
  const STATEFLAG_RULESET = 32;

  /**
   * Current state can have properties
   *
   * @var integer
   * @const
   */
  const STATEFLAG_PROPERTY = 64;

  /**
   * Current state can have value
   *
   * @var integer
   * @const
   */
  const STATEFLAG_VALUE = 128;

  /**
   * Current state can have string
   *
   * @var integer
   * @const
   */
  const STATEFLAG_STRING = 256;

  /**
   * Current state can have a function.
   * A function can be url(), format(), ...
   *
   * @var integer
   * @const
   */
  const STATEFLAG_FUNCTION = 512;

  /**
   * Current state can have variable
   *
   * @var integer
   * @const
   */
  const STATEFLAG_VARIABLE = 1024;


  /**
   * Document cannot contain string, url, property, value, @variables or @import
   * Value: STATEFLAG_COMMENT | STATEFLAG_AT_RULE | STATEFLAG_AT_RULE_VARIABLES | STATEFLAG_AT_RULE_IMPORT | STATEFLAG_RULESET
   *
   * @var integer
   * @const
   */
  const STATE_DOCUMENT = 47;

  /**
   * Comment cannot contain anything until its end.
   *
   * @var integer
   * @const
   */
  const STATE_COMMENT = 0;

  /**
   * String cannot contain anything until its end.
   * Just add a not-flagged value to make distinction beetween string and comment.
   *
   * @var integer
   * @const
   */
  const STATE_STRING = 4096;

  /**
   * Function cannot contain anything except string.
   * Value: STATEFLAG_STRING
   *
   * @var integer
   * @const
   */
  const STATE_FUNCTION = 256;

  /**
   * Declaration can contain only comment or proprety.
   * Value: STATEFLAG_COMMENT | STATEFLAG_PROPERTY
   *
   * @var integer
   * @const
   */
  const STATE_RULESET = 65;

  /**
   * Property can contain only comment, value, string or url.
   * Value: STATEFLAG_COMMENT | STATEFLAG_VALUE | STATEFLAG_STRING | STATEFLAG_FUNCTION
   *
   * @var integer
   * @const
   */
  const STATE_PROPERTY = 897;

  /**
   * @variables rule start can only contain comment or scope.
   * Note that the @variables rule can contain extra stuff before its declaration.
   * Value: STATEFLAG_COMMENT | STATEFLAG_SCOPE
   *
   * @var integer
   * @const
   */
  const STATE_AT_VARIABLES_RULE = 17;

  /**
   * @variables rule declaration can contain comment or proprety (that will be considered as variable).
   * Value: STATEFLAG_COMMENT | STATEFLAG_PROPERTY | STATEFLAG_VARIABLE
   *
   * @var integer
   * @const
   */
  const STATE_AT_VARIABLES_DECLARATION = 1090;

  /**
   * @import rule start can only contain comment, scope or url.
   * Value: STATEFLAG_COMMENT | STATEFLAG_SCOPE | STATEFLAG_FUNCTION
   *
   * @var integer
   * @const
   */
  const STATE_AT_IMPORT_RULE = 529;

  /**
   * @media rule start can only contain comment, scope, url or declaration.
   * Value: STATEFLAG_COMMENT | STATEFLAG_SCOPE
   *
   * @var integer
   * @const
   */
  const STATE_AT_MEDIA_RULE = 17;

  /**
   * @media rule contents can contain comment or declaration or extra rule.
   * Value: STATEFLAG_COMMENT | STATEFLAG_RULESET | STATEFLAG_AT_RULE
   *
   * @var integer
   * @const
   */
  const STATE_AT_MEDIA_DECLARATION = 35;

  /**
   * @page rule start can only contain comment or scope.
   * Value: STATEFLAG_COMMENT | STATEFLAG_SCOPE
   *
   * @var integer
   * @const
   */
  const STATE_AT_PAGE_RULE = 17;

  /**
   * @page rule contents can contain comment or properties.
   * Value: STATEFLAG_COMMENT | STATEFLAG_PROPERTY
   *
   * @var integer
   * @const
   */
  const STATE_AT_PAGE_DECLARATION = 65;

  /**
   * @font-face rule start can only contain comment.
   * Value: STATEFLAG_COMMENT
   *
   * @var integer
   * @const
   */
  const STATE_AT_FONTFACE_RULE = 1;

  /**
   * @font-face rule contents can contain comment or properties.
   * Value: STATEFLAG_COMMENT | STATEFLAG_PROPERTY
   *
   * @var integer
   * @const
   */
  const STATE_AT_FONTFACE_DECLARATION = 65;

  /**
   * Unknown rule start can only contain comment, scope, url or declaration.
   * Just add a not-flagged value to make distinction beetween unknown rule
   * and @import rule.
   * Value: STATEFLAG_COMMENT | STATEFLAG_SCOPE | STATEFLAG_FUNCTION
   *
   * @var integer
   * @const
   */
  const STATE_AT_UNKNOWN_RULE = 529;

  /**
   * Unknown rule contents can contain comment, ruleset, property or extra rule.
   * Value: STATEFLAG_COMMENT | STATEFLAG_AT_RULE | STATEFLAG_RULESET | STATEFLAG_PROPERTY
   *
   * @var integer
   * @const
   */
  const STATE_AT_UNKNOWN_DECLARATION = 99;

  /**
   * Tokens triggering parser processing
   *
   * @var integer
   * @const
   */
  const TOKEN_CHARS = "@{}();:\n\"'/*,";

  /**
   * List of knowns functions
   *
   * @var integer
   * @const
   */
  const TOKEN_FUNCTIONS = 'url|format';

  /**
   * Return the constant associated to a known rule name
   *
   * @param string         $rule
   * @param string         $suffix
   * @return integer
   *
   * @access public
   * @static
   */
  public static function getAtRuleConstant( $rule , $suffix )
  {
    $rule = strtoupper( preg_replace( '~[^a-z]~i' , '' , $rule ) );
    if( defined( $constant = __CLASS__.'::STATE_AT_'.strtoupper($rule).'_'.strtoupper($suffix) ) )
    {
      return constant( $constant );
    }
    return null;
  }

  /**
   * Parses the Css and returns a array of tokens.
   *
   * @param string $css
   * @return CSSMin_Token_Collection
   * @access public
   */
  public function __invoke( $css )
  {
    // Basic variables
    $c                  = null;                               // Current char
    $p                  = null;                               // Previous char
    $buffer             = '';                                 // Buffer
    $states             = array( self::STATE_DOCUMENT );      // State stack
    $c_state            = self::STATE_DOCUMENT;               // Current state
    $string_delimiter   = null;                               // String delimiter char
    $filter_spaces      = true;                               // Filter double whitespaces?
    $selectors          = array();                            // Array with collected selectors
    $rules              = array();                            // Rules stack
    $c_rule             = null;                               // Current rule
    $debug              = false;

    $r = new CSSMin_Token_Collection();  // Return tokens list

    // Prepare css
    $css = str_replace("\r\n", "\n", $css);  // Windows to Unix line endings
    $css = str_replace("\r", "\n", $css);    // Mac to Unix line endings
    while( strpos( $css, "\n\n" ) !== false ) $css = str_replace("\n\n", "\n", $css);  // Remove double line endings
    $css = str_replace("\t", " ", $css);    // Convert tabs to spaces

    // Parse css
    for( $i = 0, $imax = strlen($css); $i < $imax; $i++ )
    {
      // Fetch current character
      $c = substr($css, $i, 1);
      // Filter out double spaces
      if( $filter_spaces && $c == ' ' && $c == $p ) continue;
      // Fill buffer
      $buffer .= $c;
      if( strpos( self::TOKEN_CHARS , $c ) !== false )
      {
        // Fetch current state
        $c_state = $states[count($states) - 1];

        if( $debug )
        {
          echo '<pre>';
          print 'Current state: '.$c_state."\n";
          print 'Current char: '.$c."\n";
          print 'Current buffer: '.$buffer."\n\n";
          echo '<pre>';
        }

        // COMMENT
        // Does a comment can start
        if( ( $c_state & self::STATEFLAG_COMMENT ) && $p == '/' && $c == '*' )
        {
          $saveBuffer = substr($buffer, 0, -2); // save the buffer (will get restored with comment ending)
          $buffer = $p.$c;
          $filter_spaces  = false;
          array_push( $states, self::STATE_COMMENT );
          // Move 
          $c = substr($css, ++$i, 1);
          $buffer.=$c;
        }
        // Does a comment end
        elseif( ( $c_state === self::STATE_COMMENT ) && $p == '*' && $c == '/' )
        {
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_COMMENT , CSSMin_Core::trim($buffer) ) );
          $buffer = $saveBuffer;
          $filter_spaces  = true;
          array_pop( $states );
        }

        // STRING
        // Does a string can start
        elseif( ( $c_state & self::STATEFLAG_STRING ) && ( $c == '"' || $c == '\'' ) )
        {
          $string_delimiter = $c;
          $filter_spaces = false;
          array_push( $states, self::STATE_STRING );
        }
        // Escaped LF in string => remove escape backslash and LF
        elseif( $c_state === self::STATE_STRING && $c == "\n" && $p == '\\' )
        {
          $buffer = substr($buffer, 0, -2);
        }
        // Does a string end
        elseif( $c_state === self::STATE_STRING  && $c === $string_delimiter )
        {
          if( $p == '\\' ) // Previous char is a escape char
          {
            $count = 1;
            $i2 = $i-2;
            while( substr($css, $i2, 1) == '\\' )
            {
              $count++;
              $i2--;
            }
            // if count of escape chars is uneven => continue with string...
            if( $count % 2 )
            {
              continue;
            }
          }
          // ...else end the string
          $filter_spaces = true;
          array_pop($states);
          $string_delimiter = null;
        }

        // FUNCTION
        // Does a function can start, note that we do not need the entire CSS string
        elseif( ( $c_state & self::STATEFLAG_FUNCTION ) && $c == '(' && $this->isFunction( strtolower( substr( $css , $i - strlen( self::TOKEN_FUNCTIONS ) , strlen( self::TOKEN_FUNCTIONS ) ) ) ) )
        {
          array_push( $states, self::STATE_FUNCTION );
        }
        // Does a function end
        elseif( $c_state === self::STATE_FUNCTION && ( $c == ')' || $c == "\n" ) )
        {
          if( $p == '\\' )
          {
            continue;
          }
          array_pop($states);
        }

        // AT_RULE
        // Does an @rule can start
        elseif( ( $c_state & self::STATEFLAG_AT_RULE ) && $c == '@' )
        {
          // Which rule (most rules does not exceed 20 chars, especially @-webkit-viewport)
          $matches = array();
          preg_match( '~^@([a-z\\-]+)~ui' , substr($css, $i, 20 ) , $matches );
          $rule = $matches[1];
          
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_RULE , $rule ) );
          // Continue after rule
          $i += strlen( $rule );
          $rules[] = $rule;
          if( null !== ( $state = self::getAtRuleConstant( $rule , 'RULE' ) ) )
          {
            // This is a known rule
            array_push( $states, $state );
          }
          else
          {
            // This is a unknown rule
            array_push( $states, self::STATE_AT_UNKNOWN_RULE );
          }
          $buffer = '';
        }

        // FONT-FACE
        // @font-face block starts (no scope)
        elseif( $c_state === self::STATE_AT_FONTFACE_RULE && $c == '{' )
        {
          $buffer = '';
          array_pop( $states );
          array_push( $states, self::STATE_AT_FONTFACE_DECLARATION );
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_RULESET_START ) );
        }

        // IMPORT
        // @import block starts and ends
        elseif( $c_state === self::STATE_AT_IMPORT_RULE && $c == ';' )
        {
          $buffer = strtolower( CSSMin_Core::trim( $buffer, ';' ) );
          $scopes = implode( ',' , array_filter( array_map( array( 'CSSMin_Core' , 'trim' ) , explode( ',', $buffer ) ) ) );
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_IMPORT_SCOPES , $scopes ) );

          $buffer = '';
          array_pop( $states );
        }
        // @rule block starts
        elseif( ( $c_state & self::STATEFLAG_SCOPE ) && $c == '{' )
        {
          $buffer = strtolower( CSSMin_Core::trim( $buffer, '{' ) );
          $scopes = implode( ',' , array_filter( array_map( array( 'CSSMin_Core' , 'trim' ) , explode(',', $buffer) ) ) );
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_SCOPES , $scopes ) );

          $buffer = '';
          array_pop( $states );
          $c_rule = $rules[count($rules)-1];

          if( null !== ( $state = self::getAtRuleConstant( $c_rule , 'DECLARATION' ) ) )
          {
            // This is a known rule
            array_push( $states, $state );
          }
          else
          {
            // This is a unknown rule
            array_push( $states, self::STATE_AT_UNKNOWN_DECLARATION );
          }
          // Change current state
          $c_state = $states[count($states)-1];

          if( $c_state !== self::STATE_AT_UNKNOWN_DECLARATION && ( $c_state & self::STATEFLAG_PROPERTY ) )
          {
            // The rule is a known rule considered as a ruleset
            $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_RULESET_START ) );
          }
          elseif( $c_state === self::STATE_AT_MEDIA_DECLARATION )
          {
            // Create a known rule start token
            $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_MEDIA_START ) );
          }
          else
          {
            // Create a unknown rule start token
            $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_UNKNOWN_START ) );
          }
        }


        // @media declaration block ends
        elseif( $c_state === self::STATE_AT_MEDIA_DECLARATION && $c == '}' )
        {
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_MEDIA_STOP ) );
          $buffer = '';
          array_pop( $states );
          array_pop( $rules );
        }
        // @unknown declaration block ends
        elseif( $c_state === self::STATE_AT_UNKNOWN_DECLARATION && $c == '}' )
        {
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_AT_UNKNOWN_STOP ) );
          $buffer = '';
          array_pop( $states );
          array_pop( $rules );
        }
        
        // RULESET
        // Read selector
        elseif( ( $c_state & self::STATEFLAG_RULESET ) && $c == ',' )
        {
          $selectors[] = CSSMin_Core::trim( $buffer , ',' );
          $buffer = '';
        }
        // Start ruleset
        elseif( ( $c_state & self::STATEFLAG_RULESET ) && $c == '{' )
        {
          $selectors[]= CSSMin_Core::trim( $buffer , '{' );
          $selectors = array_filter( array_map( array( 'CSSMin_Core' , 'trim' ), $selectors ) );
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_SELECTORS , $selectors ) )
            ->add( CSSMin_Token::create( CSSMin_Token::TOKEN_RULESET_START ) );
          $buffer = '';
          $selectors  = array();
          array_push( $states, self::STATE_RULESET );
        }
        // Store proprety
        elseif( ( $c_state & self::STATEFLAG_PROPERTY ) && $c == ':' )
        {
          $property  = CSSMin_Core::trim( $buffer, ':{;' );
          $buffer = '';
          array_push( $states, self::STATE_PROPERTY );
        }
        // In this case, and only in this case, we want to outbuffer the new line character
        elseif( ( $c_state & self::STATEFLAG_VALUE ) && $c == "\n" )
        {
          $buffer = substr( $buffer , 0 , -1 );
        }
        // Store value (do not stop on new line)
        elseif( ( $c_state & self::STATEFLAG_VALUE ) && ( $c == ';' || $c == '}' ) )
        {
          // Value can be saved
          $value  = CSSMin_Core::trim( $buffer, ';}' );
          $buffer = '';
          array_pop( $states );

          if( $states[count($states)-1] & self::STATEFLAG_VARIABLE )
          {
            $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_VARIABLE, $property, $value ) );
          }
          else
          {
            $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY, $property, $value ) );
          }
          
          if( $c == '}' ) // declaration closed with a right curly brace => close ruleset
          {
            $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_RULESET_STOP ) );
            array_pop( $states );
          }
          
        }
        // End of ruleset
        elseif( ( $c_state & self::STATEFLAG_PROPERTY ) && $c == '}' )
        {
          $r->add( CSSMin_Token::create( CSSMin_Token::TOKEN_RULESET_STOP ) );
          $buffer = '';
          array_pop($states);
          // If we were in a rule, but not one that can have ruleset, pop it
          if( ! ( $states[count($states)-1] & self::STATEFLAG_RULESET ) )
          {
            array_pop( $rules );
          }
        }
      }

      $p = $c;
    }
    return $r;
  }

  /**
   * Special function detection.
   * Note that we do not need the entire CSS string.
   *
   * @param string         $css
   * @return integer
   *
   * @access public
   * @static
   */
  protected function isFunction( $css )
  { 
    foreach( explode( '|' , self::TOKEN_FUNCTIONS ) as $func ) if( substr( $css , 0 - strlen($func) ) == $func ) return true;
    return false;
  }
}
