<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Class that contract url("myURL") to url(myURL) for import rule.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: AtImportUrl.php 2 2010-09-03 15:13:57Z loops $
 */
class CSSMin_Contractor_AtImportUrl implements CSSMin_Contractor_Interface
{
  /**
   * Detect if the contraction can be apply.
   * Tips: Use an internal property to catch matches.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function detect( CSSMin_Token &$token )
  {
    return $token->getType() === CSSMin_Token::TOKEN_AT_IMPORT_SCOPES;
  }

  /**
   * Invoke the contraction on the property value.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token &$token )
  {
    // Make a temporary copy
    $url = $token->url;

    if( $url['type'] === 'path' && $url['content'] !== $url['path'] )
    {
      // Save new data
      $url['str'] = 'url('.$url['path'].')';
      $url['content'] = $url['path'];
      // Alter url informations
      $token->url = $url;
      return true;
    }
    
    if( $url['type'] === 'data' && $url['content'] !== $url['data'] )
    {
      // Save new data
      $url['str'] = 'url('.$url['data'].')';
      $url['content'] = $url['data'];
      // Alter url informations
      $token->url = $url;
      return true;
    }
    
    return false;
  }
}
