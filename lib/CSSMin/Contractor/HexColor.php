<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Class that contract color values ("#aabbcc" to "#abc").
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: HexColor.php 15 2012-09-28 07:55:13Z loops $
 */
class CSSMin_Contractor_HexColor extends CSSMin_Contractor_Base
{

  /**
   * Regular expression to apply.
   *
   * @var string
   * @access protected
   */
  protected $_regexp = '~\\#([0-9a-f]{6})~i';
  
  /**
   * Detect if the contraction can be apply.
   * Tips: Use an internal property to catch matches.
   * 
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function detect( CSSMin_Token &$token )
  {
    // This contractor cannot be apply on MS filter property
    return parent::detect( $token ) && ( ! ( $token->getType() === CSSMin_Token::TOKEN_PROPERTY && preg_match( '~^.?(ms-)?filter$~' , $token->name ) ) );
  }

  /**
   * Invoke the contraction on the property value.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token &$token )
  {
    $flag = false;
    for( $i = 0, $imax = count($this->_matches); $i < $imax; $i++ )
    {
      $this->_matches[$i][1] = strtolower($this->_matches[$i][1]);
      if( substr($this->_matches[$i][1], 0, 1) == substr($this->_matches[$i][1], 1, 1)
       && substr($this->_matches[$i][1], 2, 1) == substr($this->_matches[$i][1], 3, 1)
       && substr($this->_matches[$i][1], 4, 1) == substr($this->_matches[$i][1], 5, 1) )
      {
        $token->value = str_replace($this->_matches[$i][0], '#'.substr($this->_matches[$i][1], 0, 1).substr($this->_matches[$i][1], 2, 1).substr($this->_matches[$i][1], 4, 1) , $token->value );
        $flag = true;
      }
    }
    return $flag;
  }
}
