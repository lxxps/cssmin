<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Class that contract var("myVar") to var(myVar).
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Var.php 2 2010-09-03 15:13:57Z loops $
 */
class CSSMin_Contractor_Var extends CSSMin_Contractor_Base
{

  /**
   * Regular expression to apply.
   * Note usage of ungreedy flag.
   *
   * @var string
   * @access protected
   */
  protected $_regexp = '~(^|\\s)var\\(([^\\)]+)\\)~U';

  /**
   * Invoke the contraction on the property value.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token &$token )
  {
    for( $i = 0, $imax = count($this->_matches); $i < $imax; $i++ )
    {
      if( ( $this->_matches[$i][2]{0} === '"' && $this->_matches[$i][2]{strlen($this->_matches[$i][2])-1} === '"' )
       || ( $this->_matches[$i][2]{0} === '\'' && $this->_matches[$i][2]{strlen($this->_matches[$i][2])-1} === '\'' ) )
      {
        if( $this->_matches[$i][0]{0} === 'v' )
        {
          // If true, we can only replace the begin of the value
          $token->value = substr_replace( $token->value , 'var('.CSSMin_Core::trim($this->_matches[$i][2],'"\'').')' , 0 , strlen($this->_matches[$i][0]) );
        }
        else
        {
          // Replace everyting
          $token->value = str_replace( $this->_matches[$i][0] , ' var('.CSSMin_Core::trim($this->_matches[$i][2],'"\'').')' , $token->value );
        }
      }
    }
    return true;
  }
}
