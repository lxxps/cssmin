<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Class that contract "0 0 0 0" to "0".
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Zeros.php 8 2010-10-05 11:34:09Z loops $
 */
class CSSMin_Contractor_Zeros extends CSSMin_Contractor_Base
{

  /**
   * Regular expression to apply.
   * Note the full match.
   *
   * @var string
   * @access protected
   */
  protected $_regexp = '~^0(\\s0){1,3}$~i';

  /**
   * Detect if the contraction can be apply.
   * Tips: Use an internal property to catch matches.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function detect( CSSMin_Token &$token )
  {
    return parent::detect( $token ) && ( strpos( $token->name , 'background' ) === false ) && ( strpos( $token->name , 'font' ) === false );
  }

  /**
   * Invoke the contraction on the property value.
   * Note the full match.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token &$token )
  {
    // Note that in this case, we can have only one match
    $token->value = '0';
    return true;
  }
}
