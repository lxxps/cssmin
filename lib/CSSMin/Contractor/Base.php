<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Base class for CSS Contractor.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @abstract
 * @subversion $Id: Base.php 2 2010-09-03 15:13:57Z loops $
 */
abstract class CSSMin_Contractor_Base implements CSSMin_Contractor_Interface
{
  /**
   * Internal matches array.
   *
   * @var array
   * @access protected
   */
  protected $_matches = array();

  /**
   * Regular expression to apply.
   *
   * @var string
   * @access protected
   */
  protected $_regexp;

  /**
   * Detect if the contraction can be apply.
   * Tips: Use an internal property to catch matches.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function detect( CSSMin_Token &$token )
  {
    return ( $token->getType() === CSSMin_Token::TOKEN_PROPERTY || $token->getType() === CSSMin_Token::TOKEN_VARIABLE )
           && preg_match_all( $this->_regexp , $token->value , $this->_matches , PREG_SET_ORDER );
  }
}
