<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Class that contract url("myURL") to url(myURL).
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Url.php 15 2012-09-28 07:55:13Z loops $
 */
class CSSMin_Contractor_Url implements CSSMin_Contractor_Interface
{
  /**
   * Detect if the contraction can be apply.
   * Tips: Use an internal property to catch matches.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function detect( CSSMin_Token &$token )
  {
    return ( $token->getType() === CSSMin_Token::TOKEN_PROPERTY || $token->getType() === CSSMin_Token::TOKEN_VARIABLE )
           && count( $token->urls );
  }

  /**
   * Invoke the contraction on the property value.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token &$token )
  {
    // Make a temporary copy
    $urls = $token->urls;
    $flag = false;
    foreach( $urls as &$url )
    {
      if( $url['type'] === 'path' && $url['content'] !== $url['path'] )
      {
        // Alter value (they may be multiple url in value)
        $token->value = str_replace( $url['str'] , 'url('.$url['path'].')' , $token->value );
        // Save new data
        $url['str'] = 'url('.$url['path'].')';
        $url['content'] = $url['path'];
        $flag = true;
      }
      else if( $url['type'] === 'data' && $url['content'] !== $url['data'] )
      {
        // Alter value (they may be multiple url in value)
        $token->value = str_replace( $url['str'] , 'url('.$url['data'].')' , $token->value );
        // Save new data
        $url['str'] = 'url('.$url['data'].')';
        $url['content'] = $url['data'];
        $flag = true;
      }
    }
    if( $flag )
    {
      // Alter urls informations
      $token->urls = $urls;
    }
    return $flag;
  }
}
