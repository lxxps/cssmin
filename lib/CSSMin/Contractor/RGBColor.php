<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Class that contract RGB color values to hex ("rgb(200,60%,5)" => "#c89905").
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: RGBColor.php 2 2010-09-03 15:13:57Z loops $
 */
class CSSMin_Contractor_RGBColor extends CSSMin_Contractor_Base
{

  /**
   * Regular expression to apply.
   *
   * @var string
   * @access protected
   */
  protected $_regexp = '~rgb\\s*\\(\\s*([0-9%]+)\\s*,\\s*([0-9%]+)\\s*,\\s*([0-9%]+)\\s*\\)~i';

  /**
   * Invoke the contraction on the property value.
   *
   * @param  &CSSMin_Token $token
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token &$token )
  {
    for( $i = 0, $imax = count($this->_matches); $i < $imax; $i++ )
    {
      for( $j = 1, $jmax = count($this->_matches[$i]); $j < $jmax; $j++ )
      {
        if( strpos('%', $this->_matches[$i][$j]) !== false )
        {
          $this->_matches[$i][$j] = substr($this->_matches[$i][$j], 0, -1);
          $this->_matches[$i][$j] = (int) (256 * ($this->_matches[$i][$j] / 100));
        }
        $this->_matches[$i][$j] = str_pad( dechex($this->_matches[$i][$j]), 2, '0', STR_PAD_LEFT );
      }
      $token->value = str_replace( $this->_matches[$i][0], '#'.$this->_matches[$i][1].$this->_matches[$i][2].$this->_matches[$i][3] , $token->value );
    }
    return true;
  }
}
