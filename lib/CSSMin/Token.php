<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Token class for CSS min.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Token.php 2 2010-09-03 15:13:57Z loops $
 */
class CSSMin_Token
{

  /**
   * Token: Comment
   *
   * @var string
   * @const
   */
  const TOKEN_COMMENT = 'Comment';

  /**
   * Token: @rule
   *
   * @var string
   * @const
   */
  const TOKEN_AT_RULE = 'At-rule';

  /**
   *
   * @var string
   * @const
   */
  const TOKEN_AT_MEDIA_START = 'At-media-start';

  /**
   *
   * @var string
   * @const
   */
  const TOKEN_AT_MEDIA_STOP = 'At-media-stop';

  /**
   *
   * @var string
   * @const
   */
  const TOKEN_AT_UNKNOWN_START = 'At-unknown-start';

  /**
   *
   * @var string
   * @const
   */
  const TOKEN_AT_UNKNOWN_STOP = 'At-unknown-stop';

  /**
   * Token: Scope
   *
   * @var string
   * @const
   */
  const TOKEN_SCOPES = 'Scopes';

  /**
   * Token: Scope
   *
   * @var string
   * @const
   */
  const TOKEN_AT_IMPORT_SCOPES = 'At-import-scopes';
  
  /**
   *
   * @var string
   * @const
   */
  const TOKEN_RULESET_START = 'Ruleset-start';

  /**
   *
   * @var string
   * @const
   */
  const TOKEN_RULESET_STOP = 'Ruleset-stop';

  /**
   *
   * @var string
   * @const
   */
  const TOKEN_SELECTORS = 'Selectors';

  /**
   *
   * @var string
   * @const
   */
  const TOKEN_PROPERTY = 'Proprety';

  /**
   * Token: Variable
   *
   * @var string
   * @const
   */
  const TOKEN_VARIABLE = 'Variable';

  /**
   * @media rule name
   *
   * @var string
   * @const
   */
  const AT_RULE_MEDIA = 'media';

  /**
   * @import rule name
   *
   * @var string
   * @const
   */
  const AT_RULE_IMPORT = 'import';

  /**
   * @page rule name
   *
   * @var string
   * @const
   */
  const AT_RULE_PAGE = 'page';

  /**
   * @font-face rule name
   *
   * @var string
   * @const
   */
  const AT_RULE_FONTFACE = 'font-face';

  /**
   * @variables rule name
   *
   * @var string
   * @const
   */
  const AT_RULE_VARIABLES = 'variables';

  /**
   * Token type
   *
   * @var integer
   * @access protected
   */
  protected $_type = null;

  /**
   * Token data
   *
   * @var array
   * @access protected
   */
  protected $_data = array();

  /**
   * Create an CSSMin_Token instance
   *
   * @param string|integer $type
   * @param mixed          $var1
   * @param mixed          $var2
   * @return CSSMin_Token
   *
   * @access public
   * @static
   */
  public static function create( $type , $var1 = null, $var2 = null )
  {
    return new self( $type , $var1 , $var2 );
  }

  /**
   * Token constructor
   *
   * @param string|integer $type
   * @param mixed          $var1
   * @param mixed          $var2
   *
   * @access public
   * @throws InvalidArgumentException
   */
  public function __construct( $type , $var1 = null, $var2 = null )
  {
    $this->_type = $type;
    switch( $this->_type )
    {
      case self::TOKEN_COMMENT:
      {
        // $var1 represent comments
        $this->_data['comment'] = $var1;
      }
      break;
      case self::TOKEN_AT_RULE:
      {
        if( empty( $var1 ) )
        {
          throw new InvalidArgumentException( 'At-rule tokens must have rule type.');
        }
        // $var1 represent rule
        $this->_data['rule'] = $var1;
      }
      break;
      case self::TOKEN_SELECTORS:
      {
        // $var1 must represent selector
        if( empty( $var1 ) )
        {
          throw new InvalidArgumentException( 'Selectors tokens must have selectors.');
        }
        $this->_data['selectors'] = (array)$var1;
      }
      break;
      case self::TOKEN_SCOPES:
      {
        // $var1 must represent scopes
        if( empty( $var1 ) )
        {
          $this->_data['scopes'] = array();
        }
        else
        {
          $this->_data['scopes'] = explode( ',' , $var1 );
        }
      }
      break;
      case self::TOKEN_AT_IMPORT_SCOPES:
      {
        // $var1 must represent scopes and url
        $matches = array();
        // Use UNGREEDY flag for regular expression
        if( ! preg_match( '~url\\((.*)(?<!\\\\)\\)~U' , $var1 , $matches ) )
        {
          throw new InvalidArgumentException( 'Import scope tokens must have an url(*) string.');
        }
        $this->_data['url']['str'] = $matches[0];
        
        $this->_data['url']['type'] = preg_match( '~^\\s*data:~' , $matches[1] ) ? 'data' : 'path';
        // Trim possible string in URL
        $this->_data['url']['content'] = $matches[1];
        $this->_data['url']['data'] = $this->_data['url']['type'] === 'data' ? CSSMin_Core::trim( $matches[1] ) : null;
        $this->_data['url']['path'] = $this->_data['url']['type'] === 'path' ? CSSMin_Core::trim( $matches[1] , '"\'' ) : null;

        $var1 = CSSMin_Core::trim( str_replace( $matches[0] , '' , $var1 ) );
        // $var1 must represent scopes
        if( empty( $var1 ) )
        {
          $this->_data['scopes'] = array();
        }
        else
        {
          $this->_data['scopes'] = explode( ',' , $var1 );
        }
      }
      break;
      case self::TOKEN_PROPERTY:
      {
        // $var1 must represent property
        if( empty( $var1 ) )
        {
          throw new InvalidArgumentException( 'Property tokens must have a property name.');
        }
        if( $var2 === null )
        {
          throw new InvalidArgumentException( 'Property tokens must have a value.');
        }
        $this->_data['name'] = (string)$var1;

        // Proprety can have multiple URL, especially for font family
        $this->_data['urls'] = array();

        $matches = array();
        // Use UNGREEDY flag for regular expression
        if( preg_match_all( '~url\\((.*)(?<!\\\\)\\)~U' , $var2 , $matches , PREG_SET_ORDER ) )
        {
          for( $i = 0, $imax = count($matches); $i < $imax; $i++ )
          {
            $this->_data['urls'][$i]['str'] = $matches[$i][0];
            $this->_data['urls'][$i]['type'] = preg_match( '~^\\s*data:~' , $matches[$i][1] ) ? 'data' : 'path';
            // Trim possible string in URL
            $this->_data['urls'][$i]['content'] = $matches[$i][1];
            $this->_data['urls'][$i]['data'] = $this->_data['urls'][$i]['type'] === 'data' ? CSSMin_Core::trim( $matches[$i][1] ) : null;
            $this->_data['urls'][$i]['path'] = $this->_data['urls'][$i]['type'] === 'path' ? CSSMin_Core::trim( $matches[$i][1] , '"\'' ) : null;
          }
        }
        $this->_data['value'] = (string)$var2;
      }
      break;
      case self::TOKEN_VARIABLE:
      {
        // $var1 must represent property
        if( empty( $var1 ) )
        {
          throw new InvalidArgumentException( 'Variable tokens must have a variable name.');
        }
        if( $var2 === null )
        {
          throw new InvalidArgumentException( 'Variable tokens must have a value.');
        }
        $this->_data['name'] = (string)$var1;

        // Proprety can have multiple URL, especially for font family
        $this->_data['urls'] = array();

        $matches = array();
        // Use UNGREEDY flag for regular expression
        if( preg_match_all( '~url\\((.*)(?<!\\\\)\\)~U' , $var2 , $matches , PREG_SET_ORDER ) )
        {
          for( $i = 0, $imax = count($matches); $i < $imax; $i++ )
          {
            $this->_data['urls'][$i]['str'] = $matches[$i][0];
            $this->_data['urls'][$i]['type'] = preg_match( '~^\\s*data:~' , $matches[$i][1] ) ? 'data' : 'path';
            // Trim possible string in URL
            $this->_data['urls'][$i]['content'] = $matches[$i][1];
            $this->_data['urls'][$i]['data'] = $this->_data['urls'][$i]['type'] === 'data' ? CSSMin_Core::trim( $matches[$i][1] ) : null;
            $this->_data['urls'][$i]['path'] = $this->_data['urls'][$i]['type'] === 'path' ? CSSMin_Core::trim( $matches[$i][1] , '"\'' ) : null;
          }
        }
        $this->_data['value'] = (string)$var2;
      }
      break;
    }
  }

  /**
   * Retrieve type of the token
   *
   * @param none
   * @return integer
   *
   * @access public
   */
  public function getType()
  {
    return $this->_type;
  }
  
  /**
   * Magic function to retrieve data
   * 
   * @param string $name
   * @return mixed
   *
   * @access public
   */
  public function __get( $name )
  {
    if( ! isset($this->_data[$name]) )
    {
      throw new InvalidArgumentException( sprintf( 'Token "%s" does not have any "%s" data.' , $this->_type , $name ) );
    }
    else
    {
      return $this->_data[$name];
    }
  }

  /**
   * Magic function to set data
   *
   * @param string $name
   * @param mixed  $value
   * @return void
   *
   * @access public
   */
  public function __set( $name , $value )
  {
    if( ! isset($this->_data[$name]) )
    {
      throw new InvalidArgumentException( sprintf( 'Token "%s" does not have any "%s" data.' , $this->_type , $name ) );
    }
    $this->_data[$name] = $value;
  }
  

}
