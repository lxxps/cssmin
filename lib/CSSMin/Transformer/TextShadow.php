<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Transform text-shadow proprety.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: TextShadow.php 8 2010-10-05 11:34:09Z loops $
 */
class CSSMin_Transformer_TextShadow implements CSSMin_Transformer_Interface
{
  /**
   * Constant to determine if rule has start
   *
   * @var integer
   * @const
   */
  const RULESET_START = 1;

  /**
   * Constant to determine if rule have background
   *
   * @var integer
   * @const
   */
  const RULESET_HAS_BACKGROUND = 2;

  /**
   * Constant to determine if rule have background
   *
   * @var integer
   * @const
   */
  const RULESET_WAITING_STOP = 4;

  /**
   * Invoke the transformation on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $flag = false;
    $state = 0;
    $o_pos = null;
    $o_value = null;
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_RULESET_START )
      {
        $state = self::RULESET_START;
      }
      if( ( $state & self::RULESET_START ) && $token->getType() === CSSMin_Token::TOKEN_PROPERTY
          && in_array( $token->name , array( 'background', 'background-color', 'background-image', ) ) )
      {
        if( in_array( $token->value , array( 'none', '0', ) ) && ( $state & self::RULESET_HAS_BACKGROUND ) )
        {
          $state = $state ^ self::RULESET_HAS_BACKGROUND;
        }
        elseif( ! in_array( $token->value , array( 'none', '0', ) ) )
        {
          $state = $state | self::RULESET_HAS_BACKGROUND;
        }
      }
      if( $token->getType() === CSSMin_Token::TOKEN_PROPERTY && $token->name === 'text-shadow' )
      {
        $flag = true;
        // For these one, just add the property
        foreach( array( '-moz-text-shadow', '-webkit-text-shadow', '-khtml-text-shadow', ) as $proprety )
        {
          // Append new property (note the increment before usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , $proprety , $token->value ) , ++$pos );
        }
        // For IE, we have to determine if the parent ruleset have a background, background-color or background-image proprety
        $state = $state | self::RULESET_WAITING_STOP;
        // Save current position and value
        $o_pos = $pos;
        $o_value = $token->value;
      }
      if( $token->getType() === CSSMin_Token::TOKEN_RULESET_STOP )
      {
        if( ( $state & self::RULESET_WAITING_STOP ) && ( ! ( $state & self::RULESET_HAS_BACKGROUND ) ) )
        {
          // We do not have a background, so we can try to apply an IE shadow filter
          $offx  = (float)strtok( $o_value , ' ' );
          $offy  = (float)strtok( ' ' );
          $blur  = (float)strtok( ' ' );
          $color = strtok( ' ' );
          // Reformat color to made it understandable for IE filter
          $matches = array();
          if( preg_match( '~^\\#([0-9a-f])([0-9a-f])([0-9a-f])$~' , $color , $matches ) )
          {
            $color = '#'.$matches[1].$matches[1].$matches[2].$matches[2].$matches[3].$matches[3];
          }
          if( $blur > 0 )
          {
            // If we have a blur effect, apply it as Shadow
            // Determine angle, first vector is ( 0, 1 ) and second vector is ($offx , -$offy)
            $angle = round( rad2deg( acos( -$offy / sqrt( pow( $offy , 2 ) + pow( $offx , 2 ) ) ) ) );
            $strength = round( sqrt( pow( $offy , 2 ) + pow( $offx , 2 ) ) );
            $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , CSSMin_Core::IE_PROPERTY_PREFIX.'filter' , 'progid:DXImageTransform.Microsoft.Shadow(strength='.$strength.',color='.$color.',direction='.$angle.')' ) , ++$o_pos );
          }
          else
          {
            // Apply the effect as DropShadow
            $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , CSSMin_Core::IE_PROPERTY_PREFIX.'filter' , 'progid:DXImageTransform.Microsoft.DropShadow(offx='.$offx.',offy='.$offy.',color='.$color.')' ) , ++$o_pos );
          }
        }
        $state = 0;
      }
    }
    return $flag;
  }
  
}
