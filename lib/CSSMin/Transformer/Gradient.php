<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Transform gradient value for background.
 * Consider gradient value: gradient( x y?, color pct? , color pct? )
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: Gradient.php 8 2010-10-05 11:34:09Z loops $
 */
class CSSMin_Transformer_Gradient implements CSSMin_Transformer_Interface
{
  /**
   * Invoke the transformation on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $flag = false;
    
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_PROPERTY && in_array( $token->name , array( 'background', 'background-image', ) ) )
      {
        $matches = array();
        // Use UNGREEDY flag
        if( preg_match( '~(^|\\s)gradient\\(([^,]+)((,[^,]+)+)\\)~U' , $token->value , $matches ) )
        {
          $flag = true;
          // Remove old property
          $tokens->remove( $pos );

          // Extract point
          $point  = array_filter( array_map( array( 'CSSMin_Core' , 'trim' ) , explode( ' ' , CSSMin_Core::trim( $matches[2] ) ) ) ) + array( 'center' , 'center' );
          // Mozilla accept only "left", "right" or "center" for the first point,
          // and "top", "bottom" or "center" for the second one
          // So consider these values as possible value
          $point = array(
            in_array( $point[0] , array( 'left', 'right', 'center', ) ) ? $point[0] : 'center' ,
            in_array( $point[1] , array( 'top', 'bottom', 'center', ) ) ? $point[0] : 'center' ,
          );
          
          // Extract colors
          $colors = array_filter( array_map( array( 'CSSMin_Core' , 'trim' ) , explode( ',' , $matches[3] ) ) );

          // Create a default property with background color (the regular expression force at least one color)
          $tmp = array_filter( array_map( array( 'CSSMin_Core' , 'trim' ) , explode( ' ' , reset($colors) ) ) );
          // Append default background-color (note the increment after usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , 'background-color' , $tmp[0] ) , $pos++ );
          
          // Something is wrong with gradient proprety
          if( $point[0] === $point[1] || count($colors) < 2 ) continue;

          // Append mozilla gradient (note the increment after usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , $token->name , str_replace( $matches[0] , $this->_getMozillaGradient( $point, $colors ) , $token->value ) ) , $pos++ );
          // Append webkit gradient (note the increment after usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , $token->name , str_replace( $matches[0] , $this->_getWebkitGradient( $point, $colors ) , $token->value ) ) , $pos++ );
          // Append IE gradient (note the increment after usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , CSSMin_Core::IE_PROPERTY_PREFIX.'filter' , $this->_getIEGradient( $point, $colors ) ) , $pos++ );// Append new property (note the increment after usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , CSSMin_Core::IE_PROPERTY_PREFIX.'zoom' , '1' ) , $pos++ );
        }
      }
    }
    return $flag;
  }
  
  /**
   * Create mozilla gradient
   * 
   * @param array $point
   * @param array $colors
   * @return string
   * @access protected
   */
  protected function _getMozillaGradient( array $point , array $colors )
  {
    return '-moz-linear-gradient('.implode(' ',$point).','.implode( ',' , $colors ).')';
  }
  
  /**
   * Create webkit gradient
   * 
   * @param array $point
   * @param array $colors
   * @return string
   * @access protected
   */
  protected function _getWebkitGradient( array $point , array $colors )
  {
    // Determine webkit gradient
    // First point
    $wk_point1 = array(
      $point[0] === 'center' ? 'left' : $point[0] ,
      $point[1] === 'center' ? 'top' : $point[0] ,
    );
    // Second point
    $wk_point2 = array(
      $point[0] === 'center' ? 'left' : $point[0] ,
      $point[1] === 'center' ? 'top' : $point[0] ,
    );
    // Colors
    $wk_colors = array();
    for( $i = 0, $imax = count($colors); $i < $imax; $i++ )
    {
      if( count( $tmp = array_filter( array_map( array( 'CSSMin_Core' , 'trim' ) , explode( ' ' , $colors[$i] ) ) ) ) > 1 )
      {
        $wk_colors[$i] = array( $tmp[0] , ( (int)$tmp[1] ) / 100 );
      }
      elseif( $i === 0 )
      {
        $wk_colors[$i] = array( $tmp[0] , 0 );
      }
      elseif( $i === $imax - 1 )
      {
        $wk_colors[$i] = array( $tmp[0] , 1 );
      }
      else
      {
        $wk_colors[$i] = array( $tmp[0] );
      }
    }

    // Refactor empty color pct
    $s_i = null;
    $c_pct = 0;
    for( $i = 0; $i < $imax; $i++ )
    {
      if( count($wk_colors[$i]) !== 2 && $s_i === null )
      {
        // This color has not been determinate
        $s_i = $i;
      }
      elseif( $s_i !== null )
      {
        // Determine percent
        $tmp = round( ( ( $wk_colors[$i][1] - $c_pct ) / ( $i - $s_i ) ) + $c_pct , 2 ) * 100;
        // Create color string
        for( $j = $s_i; $j < $i; $j++ )
        {
          $wk_colors[$j] = 'color-stop('.$tmp.'%,'.$wk_colors[$j][0].')';
        }
        $wk_colors[$i] = 'color-stop('.($wk_colors[$i][1]*100).'%,'.$wk_colors[$i][0].')';
      }
      else
      {
        // Set new current percent
        $c_pct = $wk_colors[$i][1];
        $wk_colors[$i] = 'color-stop('.($wk_colors[$i][1]*100).'%,'.$wk_colors[$i][0].')';
      }
    }
    // Create webkit gradient
    return '-webkit-gradient(linear,'.implode(' ',$wk_point1).','.implode(' ',$wk_point2).','.implode(',',$wk_colors).')';
  }

  /**
   * Create IE gradient
   *
   * @param array $point
   * @param array $colors
   * @return string
   * @access protected
   */
  protected function _getIEGradient( array $point , array $colors )
  {
    // Determinate IE gradient
    $ie_color_start = reset( $colors );
    $ie_color_stop  = end( $colors );
    foreach( array( 'ie_color_start' , 'ie_color_stop' ) as $var )
    {
      $tmp = array_filter( array_map( array( 'CSSMin_Core' , 'trim' ) , explode( ' ' , ${$var} ) ) );
      $matches = array();
      if( preg_match( '~\\#([0-9a-f]){6}~' , $tmp[0] , $matches ) )
      {
        ${$var} = $matches[0];
      }
      elseif( preg_match( '~\\#([0-9a-f])([0-9a-f])([0-9a-f])~' , $tmp[0] , $matches ) )
      {
        ${$var} = '#'.$matches[1].$matches[1].$matches[2].$matches[2].$matches[3].$matches[3];
      }
      else
      {
        ${$var} = $tmp[0];
      }
    }
    if( $point[0] === 'center' )
    {
      $ie_type = '0';
      if( $point[1] === 'bottom' )
      {
        // Invert start color and stop color
        $tmp = $ie_color_start;
        $ie_color_start = $ie_color_stop;
        $ie_color_stop = $tmp;
      }
    }
    else
    {
      $ie_type = '1';
      if( $point[0] === 'right' )
      {
        // Invert start color and stop color
        $tmp = $ie_color_start;
        $ie_color_start = $ie_color_stop;
        $ie_color_stop = $tmp;
      }
    }
    // Create IE gradient (note usage of DropShadow filter)
    return 'progid:DXImageTransform.Microsoft.Gradient(GradientType='.$ie_type.',StartColorStr='.$ie_color_start.',EndColorStr='.$ie_color_stop.') '
         . 'progid:DXImageTransform.Microsoft.DropShadow(color='.$ie_color_start.',offx=0,offy=0)';
  }
  
}
