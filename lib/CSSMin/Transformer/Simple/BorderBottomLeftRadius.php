<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Transform border-bottom-left-radius proprety.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: BorderBottomLeftRadius.php 2 2010-09-03 15:13:57Z loops $
 */
class CSSMin_Transformer_Simple_BorderBottomLeftRadius extends CSSMin_Transformer_Simple_Base
{
  /**
   * Retrieve default proprety name.
   *
   * @param none
   * @return string
   *
   * @access protected
   */
  protected function _getFromProprety()
  {
    return 'border-bottom-left-radius';
  }

  /**
   * Retrieve array of proprety to duplicate with the same value.
   *
   * @param none
   * @return array
   *
   * @access protected
   */
  protected function _getToPropreties()
  {
    // Note that we kept original proprety
    return array(
      'border-bottom-left-radius' ,
      '-moz-border-radius-bottomleft' ,
      '-webkit-border-bottom-left-radius' ,
      '-khtml-bottom-left-radius' ,
    );
  }
}
