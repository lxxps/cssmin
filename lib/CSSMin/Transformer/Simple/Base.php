<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Simple transformation where a proprety is replaced by some others propreties.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @interface
 * @subversion $Id: Base.php 2 2010-09-03 15:13:57Z loops $
 */
abstract class CSSMin_Transformer_Simple_Base implements CSSMin_Transformer_Interface
{
  /**
   * Retrieve default proprety name.
   *
   * @param none
   * @return string
   *
   * @access protected
   * @abstract
   */
  abstract protected function _getFromProprety();

  /**
   * Retrieve array of proprety to duplicate with the same value.
   *
   * @param none
   * @return array
   *
   * @access protected
   * @abstract
   */
  abstract protected function _getToPropreties();

  /**
   * Invoke the transformation on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $flag = false;
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_PROPERTY && $token->name === $this->_getFromProprety() )
      {
        $flag = true;
        // Remove old proprety
        $tokens->remove( $pos );
        foreach( $this->_getToPropreties() as $proprety )
        {
          // Append new property (note the increment after usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , $proprety , $token->value ) , $pos++ );
        }
      }
    }
    return $flag;
  }
}
