<?php
/**
 * CSSMin - A CSS minifier with benefits
 *
 * --
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * --
 *
 * @package   CSSMin
 * @author    Joe Scylla <joe.scylla@gmail.com>
 * @author    Pierrot Evrard <pierrotevrard@gmail.com>
 * @copyright 2008 - 2010 Joe Scylla <joe.scylla@gmail.com>
 * @copyright 2011 - 2013 Pierrot Evrard <pierrotevrard@gmail.com>
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @version   3.0.$Id$
 */

/**
 * Transform value for white-space proprety.
 *
 * @package    CSSMin
 * @author     Pierrot Evrard <pierrotevrard@gmail.com>
 * @subversion $Id: WhiteSpace.php 8 2010-10-05 11:34:09Z loops $
 */
class CSSMin_Transformer_WhiteSpcace implements CSSMin_Transformer_Interface
{

  /**
   * Invoke the transformation on the collection list.
   *
   * @param  &CSSMin_Token_Collection
   * @return boolean
   * @access public
   */
  public function __invoke( CSSMin_Token_Collection &$tokens )
  {
    $flag = false;
    $state = 0;
    $o_pos = null;
    $o_value = null;
    foreach( $tokens as $pos => $token )
    {
      if( $token->getType() === CSSMin_Token::TOKEN_PROPERTY && $token->name === 'white-space' )
      {
        if( $token->value === 'pre-wrap' )
        {
          $flag = true;
          // Append new property (note the increment before usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , 'white-space' , '-moz-pre-wrap' ) , ++$pos );
          // Append new property (note the increment before usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , 'white-space' , '-webkit-pre-wrap' ) , ++$pos );
          // Append new property (note the increment before usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , 'white-space' , '-khtml-pre-wrap' ) , ++$pos );
          // Append new property (note the increment before usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , 'white-space' , '-pre-wrap' ) , ++$pos );
          // Append new property (note the increment before usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , 'white-space' , '-o-pre-wrap' ) , ++$pos );
          // Append new property (note the increment before usage)
          $tokens->append( CSSMin_Token::create( CSSMin_Token::TOKEN_PROPERTY , CSSMin_Core::IE_PROPERTY_PREFIX.'word-wrap' , 'break-word' ) , ++$pos );
        }
      }
    }
    return $flag;
  }
  
}
