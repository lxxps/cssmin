<?php header('Content-Type: text/css; charset=utf-8'); ?>

/**
 * Font generator file
 * 
 * @package    vanilla
 * @subpackage form
 * @author     Loops <evrard@h2a.lu>
 * @version    SVN: $Id: _generator.css.php 30 2013-11-14 11:19:21Z loops $
 */

<?php // each font is represented by two value, the font name, and the font file prefix ?>
<?php // use fontsquirel fontface kit to get files and font name ?>
<?php foreach( array( 
  array( 'bebasneue' , 'bebasneue' ) ,
  array( 'casloncp' , 'casloncp' ) ,
) as $font ): ?>

@font-face {
  font-family: '<?php echo $font[0]; ?>';
  src: url('<?php echo $font[1]; ?>-webfont.eot');
}

@font-face {
  font-family: '<?php echo $font[0]; ?>';
  src: url('<?php echo $font[1]; ?>-webfont.eot?#iefix') format('embedded-opentype'),
       url(data:font/woff;charset=utf-8;base64,<?php echo base64_encode( file_get_contents( dirname(__FILE__).'/'.$font[1].'-webfont.woff' ) ); ?>) format('woff'),
<?php /** / ?>       url(data:font/truetype;charset=utf-8;base64,<?php echo base64_encode( file_get_contents( dirname(__FILE__).'/'.$font[1].'-webfont.ttf' ) ); ?>) format('truetype'), <?php /**/ ?>
       url('<?php echo $font[1]; ?>-webfont.ttf') format('truetype'),
       url('<?php echo $font[1]; ?>-webfont.svg#<?php echo $font[0]; ?>') format('svg');
  font-weight: normal;
  font-style: normal;
}

<?php endforeach; ?>