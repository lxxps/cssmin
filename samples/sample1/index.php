<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>CSSMin - Sample 1 - Real case study</title>
  </head>
  <body>
<?php
  $src = dirname(__FILE__).'/css/main.css';

  require dirname(__FILE__).'/../../lib/CSSMin/bootstrap.inc.php';
  
  $result = CSSMin::minify( $src )
?>
  <h1>Sample 1: <?php echo basename( $src ); ?> - Real case study</h1>
  <p>
    The file import a lot of stuff (fonts, big backgrounds, small backgrounds…) and you also have some empty media rules and properties.<br />
    I also use some backslash in selectors (.\:fixed, .col-1\/2…) and they are preserved.<br />
  </p>
  <pre style="white-space:normal;word-wrap:break-word;"><?php echo nl2br( htmlspecialchars( $result ) ) ?></pre>
  </body>
</html>
